## x.xx [Code/Art/Design/Other] [Type] title description

**Build version:** v. x.xx

**Test device:**

**Tester:** 

### Issue description: 
More detailed description, keep it to a few sentences.
### Expected results: 
More detailed description, keep it to a few sentences.
### Reproduction steps:
 - Step by step
 - instructions
 - on how to 
 - reproduce the bug

**Reproduction rate:**
Percentage for reproducing the bug

**Comments:**
Any extra info that might be relevant.