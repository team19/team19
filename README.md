Wiki :             <a href="https://gitlab.com/team19/team19/commits/pages"><img alt="build status" src="https://gitlab.com/team19/team19/badges/pages/build.svg" /></a>

Documentation :   <a href="https://gitlab.com/team19/team19-documentation/commits/master"><img alt="build status" src="https://gitlab.com/team19/team19-documentation/badges/master/build.svg" /></a>



---

# Bluepix - Team19

## 3rd year group project, Abertay University.

## V&A Brief - 3D vertical Puzzle Game.

---

### Bluepix Project Wiki

https://team19.gitlab.io/team19/

### Bluepix Project Documentation

https://team19.gitlab.io/team19-documentation/

