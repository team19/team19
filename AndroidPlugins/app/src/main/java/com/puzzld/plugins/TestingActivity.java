package com.puzzld.plugins;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.puzzld.detectautorotate.AutoRotateUtil;

public class TestingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing);

    }
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(hasFocus){
            String text = "AutoRotation Enabled: " +AutoRotateUtil.IsAutoRotateEnabled(this);
            TextView txtAutoRotate = (TextView)findViewById(R.id.AutoRotation);
            txtAutoRotate.setText(text);
        }

    }
}
