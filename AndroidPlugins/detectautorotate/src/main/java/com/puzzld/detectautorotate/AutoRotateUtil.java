package com.puzzld.detectautorotate;

import android.app.Activity;
import android.provider.Settings;
import android.util.Log;

/** Gives access to whether the user has AutoRotate turned on or off on Android.
 * @author Stewart McCready
 *
 * Gives access to whether the user has AutoRotate turned on/off on Android. This is useful for
 * Unity as it does not track whether the user has disabled autoRotate itself.
 */
public class AutoRotateUtil {
    /** Tells you if the users auto rotation is enabled.
     * @param activity Should be the currently running Activity, i.e The Unity Player activity
     * @return Whether the user has Auto Rotate enabled or disabled on Android.
     */
    public static boolean IsAutoRotateEnabled(Activity activity){
        boolean isAutoRotateEnabled = false;
        try
        {
            isAutoRotateEnabled = (Settings.System.getInt(activity.getContentResolver(),
                    Settings.System.ACCELEROMETER_ROTATION)==1);
        }
        catch (Exception e)
        {
            Log.i("UserRotationUtil", "Couldn't retrieve auto rotation setting: " + e.getMessage());
        }
        return isAutoRotateEnabled;
    }

}
