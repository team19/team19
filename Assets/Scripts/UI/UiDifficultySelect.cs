﻿using UnityEngine;
using UnityEngine.UI;

using LittleLot;

/// <summary>
/// The Difficulty Select displays detailed information about the currently selected level and difficulty.
/// </summary>
public class UiDifficultySelect : MonoBehaviour {

	private const int kMaxDifficulties = 2;

	[SerializeField]
	Text levelName = null;

	[SerializeField]
	UiImageToggle[] stars = new UiImageToggle[3];

	[SerializeField]
	Image levelDifficultyHangar = null;

	[SerializeField]
	Sprite[] hangingBanners = new Sprite[3];

	[SerializeField]
	Image capsuleImage = null;

	[SerializeField]
	Animator lightBulbStamp = null;

	[SerializeField]
	Button playButton = null;

	[SerializeField]
	Button previousDifficultyButton = null;

	[SerializeField]
	Button nextDifficultyButton = null;

	private Animator panelAnimator_ = null;

	private LevelInfo storedLevelInfo_ = null;

	private string selectedDifficultySceneName_ = "";

	private int currentDifficulty_ = 0;

	/// <summary>
	/// Setups all the event listeners for panel movement and level selection.
	/// </summary>
	private void OnEnable() {
		// Makes sure the animator is set.
		if (!panelAnimator_) {
			panelAnimator_ = GetComponent<Animator>();
		}
		// Add all listeners.
		playButton.onClick.AddListener(OnPlayClicked);
		previousDifficultyButton.onClick.AddListener(PreviousDifficulty);
		nextDifficultyButton.onClick.AddListener(NextDifficulty);

		EventManagerLevelSelect.OnDifficultySelectOpened += ShowDifficultySelect;
		EventManagerLevelSelect.OnPopulateDifficultySelect += PopulateDifficultySelect;

	}

	/// <summary>
	/// Ensures all Listeners have been removed and disabled.
	/// </summary>
	private void OnDisable() {
		playButton.onClick.RemoveListener(OnPlayClicked);
		previousDifficultyButton.onClick.RemoveListener(PreviousDifficulty);
		nextDifficultyButton.onClick.RemoveListener(NextDifficulty);

		EventManagerLevelSelect.OnDifficultySelectOpened -= ShowDifficultySelect;
		EventManagerLevelSelect.OnPopulateDifficultySelect -= PopulateDifficultySelect;
	}

	/// <summary>
	/// Sets the Difficulty Select to the provided level info.
	/// Bases available information on the mostRecentSave so it only shows discovered and relevent information.
	/// </summary>
	/// <param name="newInfo"> The Level info to build the Difficulty Select from. </param>
	private void PopulateDifficultySelect(LevelInfo newInfo) {
		storedLevelInfo_ = newInfo;
		currentDifficulty_ = 0;
		SaveData mostRecentSave = SaveProgressSystem.RecentSave();
		if (mostRecentSave.availableLevelList.ContainsKey(storedLevelInfo_.sceneNames[currentDifficulty_])) {
			levelName.text = storedLevelInfo_.displayName;
			capsuleImage.sprite = storedLevelInfo_.displayIcon;
			currentDifficulty_++;
			if (mostRecentSave.availableLevelList.ContainsKey(storedLevelInfo_.sceneNames[currentDifficulty_])) {
				currentDifficulty_++;
			}
		}
		else {
			levelName.text = storedLevelInfo_.hiddenName;
			capsuleImage.sprite = storedLevelInfo_.hiddenIcon;
		}

		UpdateDisplayedDifficulty();
	}

	/// <summary>
	/// Moves to the next available level difficulty.
	/// Updates the Diffculty Select Ui.
	/// </summary>
	private void NextDifficulty() {
		SoundBank.PlayButtonClick();
		currentDifficulty_++;
		currentDifficulty_ = Mathf.Clamp(currentDifficulty_, 0, kMaxDifficulties);
		UpdateDisplayedDifficulty();
	}

	/// <summary>
	/// Moves to the previous available level difficulty.
	/// Updates the Diffculty Select Ui.
	/// </summary>
	private void PreviousDifficulty() {
		SoundBank.PlayButtonClick();
		currentDifficulty_--;
		currentDifficulty_ = Mathf.Clamp(currentDifficulty_, 0, kMaxDifficulties);
		UpdateDisplayedDifficulty();
	}

	/// <summary>
	/// Updates the level Difficulty Ui to match the selected Level Difficuty.
	/// Replays any animations etc.
	/// </summary>
	private void UpdateDisplayedDifficulty() {
		levelDifficultyHangar.sprite = hangingBanners[currentDifficulty_];
		selectedDifficultySceneName_ = storedLevelInfo_.sceneNames[currentDifficulty_];

		// Turn off all the stars.
		foreach (var star in stars) {
			star.TurnOff();
		}
		// turn off lightBulbStamp
		lightBulbStamp.Play("greyedOutLightBulbStamp", 0, 0);


		SaveData mostRecentSave = SaveProgressSystem.RecentSave();

		if (mostRecentSave.availableLevelList.ContainsKey(storedLevelInfo_.sceneNames[currentDifficulty_])) {
			LevelData levelData = mostRecentSave.availableLevelList[storedLevelInfo_.sceneNames[currentDifficulty_]];

			// Turn on stars, based on last score in that level.
			stars[0].TurnOn();
			if (levelData.movesTaken <= storedLevelInfo_.twoStarMaxMoves[currentDifficulty_]) {
				stars[1].TurnOn();
			}

			if (levelData.movesTaken <= storedLevelInfo_.threeStarMaxMoves[currentDifficulty_]) {
				stars[2].TurnOn();
			}

			if (levelData.itemsCollected > 0) {
				lightBulbStamp.Play("lightBulbStamp", 0, 0);
			}
		}

		previousDifficultyButton.interactable = true;
		nextDifficultyButton.interactable = true;

		if (currentDifficulty_ - 1 < 0) {
			previousDifficultyButton.interactable = false;
		}
		else if (currentDifficulty_ + 1 > kMaxDifficulties) {
			nextDifficultyButton.interactable = false;
		}

		playButton.interactable = false;
		playButton.GetComponentInChildren<Text>().text = "LOCKED";
		int previousLevel = Mathf.Clamp(currentDifficulty_ - 1, 0, kMaxDifficulties);
		// Enable play button if previous level has save data, or first level
		if (currentDifficulty_ == 0 || mostRecentSave.availableLevelList.ContainsKey(storedLevelInfo_.sceneNames[previousLevel])) {
			playButton.interactable = true;
			playButton.GetComponentInChildren<Text>().text = "OPEN";
		}
	}

	/// <summary>
	/// Play Animation for Difficulty Select.
	/// </summary>
	private void ShowDifficultySelect() {
		panelAnimator_.enabled = true;
		// Play selected animation.
		panelAnimator_.Play("difficultySelectSpawn", 0, 0);
	}

	/// <summary>
	/// Checks which level has been selected and loads it when the play button is pressed.
	/// </summary>
	private void OnPlayClicked() {
		SoundBank.PlayButtonClick();
		SaveData mostRecentSave = SaveProgressSystem.RecentSave();
		if (!mostRecentSave.finishedOnboarding) {
			mostRecentSave.finishedOnboarding = true;
			SaveProgressSystem.SaveProgress(mostRecentSave);
		}

		playButton.interactable = false;

		LoadingTransitionController.AnimatedLoadSceneAsync(selectedDifficultySceneName_);
	}
}
