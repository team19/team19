﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Once the player has played all the levels it brings up the Endscreen.
/// </summary>
[RequireComponent(typeof(Animator))]
public class UiEndScreen : MonoBehaviour {

	[SerializeField]
	MapInfo mapInfo = null;

	[SerializeField]
	Button closeEndScreenButton = null;

	private Animator animator_ = null;

	/// <summary>
	/// Adds the close button listener, and sets up the animator.
	/// </summary>
	private void OnEnable() {
		// Makes sure the animator is set.
		if (!animator_) {
			animator_ = GetComponent<Animator>();
		}
		closeEndScreenButton.onClick.AddListener(CloseEndscreen);

		EventManagerDebug.OnFakeWin += ShowEndScreen;
	}

	/// <summary>
	/// Removes the close button listener
	/// </summary>
	private void OnDisable() {
		closeEndScreenButton.onClick.RemoveListener(CloseEndscreen);

		EventManagerDebug.OnFakeWin -= ShowEndScreen;
	}

	/// <summary>
	/// Checks for if the player has finished all levels and displays the end screen if they have.
	/// </summary>
	private void Start() {
		if (HasFinishedAllLevels()) {
			ShowEndScreen();
		}
	}

	/// <summary>
	/// Checks all the levels stored in map info against the most recent save.
	/// Finds if they have finished all the levels, and have not viewed the endscreen yet.
	/// </summary>
	/// <returns>
	/// Returns true if all levels are found in the save data,
	///  and it is the first time viewing the endscreen.
	///  </returns>
	private bool HasFinishedAllLevels() {
		SaveData mostRecentSave = SaveProgressSystem.RecentSave();

		// Go through all the catgories looking for it in the save file.
		foreach (Category category in mapInfo.categories) {
			// Go through all the catergories levels.
			foreach (LevelInfo level in category.levelInfos) {
				// Go through the levels scenes.
				foreach (string sceneName in level.sceneNames) {
					// If it can't find the scene in the level data then it hasn't completed the level.
					if (!mostRecentSave.availableLevelList.ContainsKey(sceneName)) {
						return false;
					}
				}
			}
		}
		// It has found save data for all the levels in the map info therefore has finished all the levels.
		// Checks if they have seen it before.
		if (mostRecentSave.hasViewedEndscreen) {
			return false;
		}
		mostRecentSave.hasViewedEndscreen = true;
		SaveProgressSystem.SaveProgress(mostRecentSave);
		return true;
	}

	/// <summary>
	/// Displays the end screen using the animator.
	/// </summary>
	private void ShowEndScreen() {
		animator_.SetBool("isInView", true);
	}

	/// <summary>
	/// Closes the end screen ui panel.
	/// </summary>
	private void CloseEndscreen() {
		animator_.SetBool("isInView", false);
	}
}
