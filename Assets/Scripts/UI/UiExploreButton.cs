﻿using UnityEngine;

using LittleLot;

/// <summary>
/// Handles Start screen button.
/// </summary>
public class UiExploreButton : MonoBehaviour {

	/// <summary>
	/// Start Buttons onClick event loads the map screen.
	/// </summary>
	public void OnClickStart() {
		SoundBank.PlayButtonClick();
		LoadingTransitionController.AnimatedLoadSceneAsync("MapScreen", "AnimatedLoadingScreen");
	}

}
