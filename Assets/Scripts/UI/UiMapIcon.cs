﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Ensures the MapIcon is correctly named.
/// This allows the LevelItems to connect properly to the MapIcon,
/// Ensuring that all level items have a corresponding map icon.
/// </summary>
[RequireComponent(typeof(Button))]
public class UiMapIcon : MonoBehaviour {

	[SerializeField]
	LevelInfo levelInfo = null;

	[SerializeField]
	Image mapPin = null;

	/// <summary>
	/// Makes sure the mapicon is named correctly on launch.
	/// This is encase the editor script never runs in the scene, it 
	/// ensures it runs on firstlaunch, making everything match.
	/// </summary>
	private void Awake() {
		UpdateNameToLevelInfo();
	}

	/// <summary>
	/// Checks if it is set to the correct name for the level info.
	/// If able changes it to match the level Info's display name, otherwise logs the error.
	/// </summary>
	public void UpdateNameToLevelInfo() {
		if (levelInfo) {
			gameObject.name = levelInfo.displayName;
		}
		else {
			Debug.Log("LevelInfo Not Set:" + gameObject.name);
		}
	}

	/// <summary>
	/// Hides the map pin.
	/// </summary>
	public void TurnOff() {
		mapPin.enabled = false;
	}

	/// <summary>
	/// Shows the map pin.
	/// </summary>
	public void TurnOn() {
		mapPin.enabled = true;
	}

	/// <summary>
	/// Updates the MapIcon's name to match LevelInfo in editor.
	/// </summary>
	private void OnValidate() {
		UpdateNameToLevelInfo();
	}
}
