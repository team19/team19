﻿using System.Collections;

using UnityEngine;

/// <summary>
/// Simple pop up class, uses the prefab for popups.
/// It should be created when a collectable is collected to create a simple animation that disables
///  after display time.
/// </summary>
public class UiCollectablePopUp : MonoBehaviour {

	[SerializeField]
	float displayTime =1.0f;

	/// <summary>
	/// Starts a coroutine to disable the popup after the display time has ended.
	/// </summary>
	private void Start() {
		StartCoroutine(WaitThenDestroy());
	}

	/// <summary>
	/// Coroutine function, that will destroy the popup after the display time has passed.
	/// </summary>
	IEnumerator WaitThenDestroy() {
		yield return new WaitForSeconds(displayTime);
		gameObject.SetActive(false);
	}
}
