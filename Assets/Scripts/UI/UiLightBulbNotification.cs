﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Ui for the number of unrevealed LightBulb Info's.
/// </summary>
public class UiLightBulbNotification : MonoBehaviour {

	[SerializeField]
	Text lightBulbNotificationText = null;
	
	/// <summary>
	/// Update the number of unrevealed lightBulbs in the notification.
	/// The notification will be disabled/hidden if there is none unrevealed.
	/// </summary>
	public void UpdateNotificationText(int lightBulbsUnrevealed) {		
		if (lightBulbsUnrevealed <= 0) {
			gameObject.SetActive(false);
		}else {
			gameObject.SetActive(true);
			lightBulbNotificationText.text = lightBulbsUnrevealed.ToString();
		}
	}
}
