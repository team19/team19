﻿using UnityEngine;

/// <summary>
/// Controls and generates the list of available levels selection panel.
/// </summary>
public class UiLevelSelectionPanel : MonoBehaviour {

	[SerializeField]
	UiLevelItem templateLevelItem = null;

	[SerializeField]
	UiCategoryItem templateCategoryBanner = null;

	[SerializeField]
	MapInfo mapInfo = null;

	[SerializeField]
	float categorySpacing = 32.0f;

	[SerializeField]
	float levelSpacing = 144.0f;

	[SerializeField]
	float endPadding = 10.0f;

	/// <summary>
	/// Generates all the selectable Items based on templateLevelItem.
	/// 
	/// Populates them with the Level Info (generates all the set Level Info to Ui Items),
	/// also ensures they are in the correct place.
	/// </summary>
	private void Start() {
		// Ensures the sub-view is always at the top.
		transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);

		RectTransform levelItemRectTransform = templateLevelItem.GetComponent<RectTransform>();
		RectTransform categoryItemRectTransform = templateCategoryBanner.GetComponent<RectTransform>();

		float currentPosition = 0;

		foreach (Category category in mapInfo.categories) {
			GameObject newCategoryBanner = Instantiate(templateCategoryBanner.gameObject, transform);
			newCategoryBanner.GetComponent<UiCategoryItem>().SetCategory(category);

			// update the position to the bottom of the current category.
			currentPosition -= categorySpacing;

			foreach (LevelInfo levelInfo in category.levelInfos) {
				levelItemRectTransform.localPosition = new Vector3(
					levelItemRectTransform.localPosition.x,
					currentPosition,
					levelItemRectTransform.localPosition.z);
				levelItemRectTransform.ForceUpdateRectTransforms();

				GameObject newLevelItem = Instantiate(templateLevelItem.gameObject, transform);
				newLevelItem.GetComponent<UiLevelItem>().PopulateItem(levelInfo);
				// Ensures all level items are behind the category banners,
				// and the preceding level items. (helps with texture overlay)
				newLevelItem.transform.SetSiblingIndex(0);

				// update the position to the bottom of the current level.
				currentPosition -= levelSpacing;
			}
			// add the extra padding after a list of levels.
			currentPosition -= endPadding;

			categoryItemRectTransform.localPosition = new Vector3(
					levelItemRectTransform.localPosition.x,
					currentPosition,
					levelItemRectTransform.localPosition.z);
			categoryItemRectTransform.ForceUpdateRectTransforms();
		}


		RectTransform rectTransform = GetComponent<RectTransform>();

		LittleLot.ScrollViewUtil.ResizeAndClampToTop(rectTransform, Mathf.Abs(currentPosition));
	}
}
