﻿using System.Collections;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using LittleLot;

/// <summary>
/// Fades in a Win screen based on animation, updates and sets all the win screen ui aswell. 
/// </summary>
[RequireComponent(typeof(Animator))]
public class UiWinScreen : MonoBehaviour {

	private const string kMapSceneName_ ="MapScreen";

	[SerializeField]
	Button continueButton = null;
	[SerializeField]
	Button restartButton = null;
	[SerializeField]
	Button mapButton = null;

	[SerializeField]
	Image starBar = null;
	[SerializeField]
	Animator[] stars = new Animator[3];

	[SerializeField]
	Button stampButton = null;

	[SerializeField]
	MapInfo mapInfo = null;

	[SerializeField]
	GameObject collectableTooltip = null;

	private float starDelay_ = 0.4f;

	private float stampDelay_ = 1.2f;

	private int currentDifficulty_ = 0;

	private LevelInfo currentLevel_;

	private int moveCount_ =0;

	private int collectedCount_ =0;

	private Animator animator_ = null;

	private string currentScene_ = kMapSceneName_;

	private string nextScene_ = kMapSceneName_;

	private bool isLastScreen_ =false;

	private float starBarFillIncrement_ = 0.0f;
	private float star1FillIncrement_ = 0.0f;
	private float star2FillIncrement_ = 0.0f;
	private float star3FillIncrement_ = 0.0f;

	private bool isFirstCollectable_ = false;

	/// <summary>
	/// Ensure event listerners are added, and make sure the object is not active in the scene.
	/// </summary>
	private void Start() {
		EventManagerWinScreen.OnCameraInPos += ShowWinScreen;
		EventManagerCollectable.OnItemCollected += IncreaseCollectedCount;

		EventManagerWinScreen.OnMoveCountDownTotalMoves += CalculateFillIncrements;

		EventManagerWinScreen.OnMoveCountDown += IncreaseMoveCount;

		EventManagerWinScreen.OnMoveCountDownFinished += UpdateDisplayedScore;

		EventManagerLoadingSystem.OnLoadingFinished += FinishLoading;

		EventManagerDebug.OnFakeWin += FakeWin;

		continueButton.onClick.AddListener(LoadNextScene);
		restartButton.onClick.AddListener(RestartScene);
		mapButton.onClick.AddListener(LoadMapScene);
		stampButton.onClick.AddListener(ToggleCollectableTooltip);

		// Makes sure the animator is set.
		if (!animator_) {
			animator_ = GetComponent<Animator>();
		}
		// Make sure setup finishes when there is no loading system (i.e. ran in editor from scene)
		if (!LoadingTransitionController.HasLoadingSystem()) {
			FinishLoading();
		}

		TurnOffInteraction();
		collectableTooltip.SetActive(false);
		stampButton.interactable = false;
	}

	/// <summary>
	/// Make Sure all Event Listeners have been removed when destroying.
	/// </summary>
	private void OnDestroy() {
		EventManagerWinScreen.OnCameraInPos -= ShowWinScreen;
		EventManagerCollectable.OnItemCollected -= IncreaseCollectedCount;

		EventManagerWinScreen.OnMoveCountDownTotalMoves -= CalculateFillIncrements;

		EventManagerWinScreen.OnMoveCountDown -= IncreaseMoveCount;

		EventManagerWinScreen.OnMoveCountDownFinished -= UpdateDisplayedScore;

		EventManagerLoadingSystem.OnLoadingFinished -= FinishLoading;

		EventManagerDebug.OnFakeWin -= FakeWin;

		continueButton.onClick.RemoveListener(LoadNextScene);
		restartButton.onClick.RemoveListener(RestartScene);
		mapButton.onClick.RemoveListener(LoadMapScene);
		stampButton.onClick.RemoveListener(ToggleCollectableTooltip);
	}

	/// <summary>
	/// Sets the current scene and next scene once the scene has fully loaded.
	/// Ensuring the scene has been set to active.
	/// </summary>
	private void FinishLoading() {
		currentScene_ = SceneManager.GetActiveScene().name;
		nextScene_ = GetNextScene();
	}

	/// <summary>
	/// Gets the next possible scene from the map's category and level info.
	/// If it cannot find a next level it returns the mapscreen instead.
	/// And sets isLastScreen to true.
	/// </summary>
	/// <returns> The name of the next scene to load. </returns>
	private string GetNextScene() {
		// go through all the catgories look for the matching level.
		foreach (Category category in mapInfo.categories) {
			foreach (LevelInfo level in category.levelInfos) {
				for (int i = 0; i < level.sceneNames.Length; i++) {
					if (SceneManager.GetActiveScene().name == level.sceneNames[i]) {
						currentDifficulty_ = i;
						currentLevel_ = level;
						i++;
						// check to see if it has another scene.
						if (i < level.sceneNames.Length) {
							return level.sceneNames[i];
						}
						else {
							isLastScreen_ = true;
							return kMapSceneName_;
						}
					}
				}
			}
		}
		currentLevel_ = ScriptableObject.CreateInstance<LevelInfo>();
		currentDifficulty_ = 0;
		Debug.LogError("Scene not found in Map Description " + SceneManager.GetActiveScene().name + "\nUsing default Level Info Settings");
		// just incase we dont find any scenes we just return the map scene.
		return kMapSceneName_;
	}

	/// <summary>
	/// Show all the UI required for the Win Screen.
	/// Setup star bar fill increments based on star position, and star move brakcets.	
	/// </summary>
	private void ShowWinScreen() {
		animator_.SetBool("isInView", true);
	}

	/// <summary>
	/// Calculates the increment amount for filling the stat bar for each star.
	/// </summary>
	/// <param name="totalCount"> The total number of moves used for completing the level. </param>
	private void CalculateFillIncrements(int totalCount) {
		starBar.fillAmount = 0.0f;
		float star3X = stars[2].GetComponent<RectTransform>().anchoredPosition.x;
		float star2X = stars[1].GetComponent<RectTransform>().anchoredPosition.x;
		float barWidth = starBar.rectTransform.rect.width;
		star3FillIncrement_ = ((barWidth - star3X) / barWidth) / currentLevel_.threeStarMaxMoves[currentDifficulty_];

		star2FillIncrement_ = ((star3X - star2X) / barWidth) / (currentLevel_.twoStarMaxMoves[currentDifficulty_] - currentLevel_.threeStarMaxMoves[currentDifficulty_]);

		star1FillIncrement_ = (star2X / barWidth) / (totalCount - currentLevel_.twoStarMaxMoves[currentDifficulty_]);
	}

	/// <summary>
	/// Increases the number of moves taken for the level, and increase the star bar.		
	/// </summary>	
	private void IncreaseMoveCount() {
		moveCount_++;

		if (moveCount_ <= currentLevel_.threeStarMaxMoves[currentDifficulty_]) {
			starBarFillIncrement_ = star3FillIncrement_;
		}
		else if (moveCount_ <= currentLevel_.twoStarMaxMoves[currentDifficulty_]) {
			starBarFillIncrement_ = star2FillIncrement_;
		}
		else {
			starBarFillIncrement_ = star1FillIncrement_;
		}

		starBar.fillAmount += starBarFillIncrement_;
	}

	/// <summary>
	/// Increases the number of collected collectables for the level.	
	/// </summary>	
	private void IncreaseCollectedCount() {
		collectedCount_++;
	}

	/// <summary>
	/// Updates the UI display of the score, stars/stamps.
	/// Starting the animation of all stars and stamps gained,as well as enabling the Ui itneraction.
	/// </summary>
	private void UpdateDisplayedScore() {
		restartButton.interactable = true;
		mapButton.interactable = true;
		continueButton.interactable = true;

		if (collectedCount_ > 0) {
			StartCoroutine(AnimateStamp(stampDelay_));
		}

		stars[0].SetBool("hasGained", true);
		SoundBank.PlayStarGained();
		if (moveCount_ <= currentLevel_.twoStarMaxMoves[currentDifficulty_]) {
			StartCoroutine(AnimateStar(starDelay_, 1));
		}

		if (moveCount_ <= currentLevel_.threeStarMaxMoves[currentDifficulty_]) {
			StartCoroutine(AnimateStar(starDelay_ * 2, 2));
		}

		SaveCurrentProgress();
	}

	/// <summary>
	/// Starts the StarGained animation after a delay, on the indexed star.
	/// </summary>
	/// <param name="delay"> Delay in seconds before animation starts. </param>
	/// <param name="starIndex"> Star index to play animation on. </param>	
	private IEnumerator AnimateStar(float delay, int starIndex) {
		yield return new WaitForSeconds(delay);
		stars[starIndex].SetBool("hasGained", true);
		SoundBank.PlayStarGained();
	}

	/// <summary>
	/// Starts the StampGained animation after a delay.
	/// </summary>
	/// <param name="delay"> Delay in seconds before animation starts. </param>
	private IEnumerator AnimateStamp(float delay) {
		yield return new WaitForSeconds(delay);
		stampButton.interactable = true;
		SoundBank.PlayStamped();
		if (isFirstCollectable_) {
			// Show the collectable tooltip.
			collectableTooltip.SetActive(true);
		}
		else {
			collectableTooltip.SetActive(false);
		}
	}

	/// <summary>
	/// Disables interaction with the ui elements whilst loading.
	/// </summary>
	private void TurnOffInteraction() {
		restartButton.interactable = false;
		mapButton.interactable = false;
		continueButton.interactable = false;
	}

	/// <summary>
	/// Load the scene next scene in the mapInfo.
	/// </summary>
	private void LoadNextScene() {
		SoundBank.PlayButtonClick();
		TurnOffInteraction();
		if (isLastScreen_) {
			LoadingTransitionController.AnimatedLoadSceneAsync(nextScene_, "AnimatedLoadingScreen");
		}
		else {
			LoadingTransitionController.AnimatedLoadSceneAsync(nextScene_);
		}
	}

	/// <summary>
	/// Reload the Current Scene Scene.
	/// </summary>
	private void RestartScene() {
		SoundBank.PlayButtonClick();
		TurnOffInteraction();
		LoadingTransitionController.AnimatedLoadSceneAsync(currentScene_);
	}

	/// <summary>
	/// Loads the map screen.
	/// </summary>
	private void LoadMapScene() {
		SoundBank.PlayButtonClick();
		TurnOffInteraction();
		LoadingTransitionController.AnimatedLoadSceneAsync(kMapSceneName_, "AnimatedLoadingScreen");
	}

	/// <summary>
	/// Saves the current progress made in the game.
	/// Compares the new scores to old scores and updates the save data.
	/// Adding or overriding if neccessary, also checks if the have gained there 
	/// first collectable, updateing isFirstCollectable_.
	/// </summary>
	private void SaveCurrentProgress() {
		SaveData mostRecentSave = SaveProgressSystem.RecentSave();

		// iterate through all the saved levels and count their collected objects.
		int newTotalCollected = 0;
		foreach (var item in mostRecentSave.availableLevelList) {
			newTotalCollected += item.Value.itemsCollected;
		}
		if (newTotalCollected == 0) {
			isFirstCollectable_ = true;
		}
		else {
			isFirstCollectable_ = false;
		}

		// Generate new level data from the current win.
		LevelData newLevelData = new LevelData();
		newLevelData.itemsCollected = collectedCount_;
		newLevelData.movesTaken = moveCount_;

		// check if they have already done this level.
		if (mostRecentSave.availableLevelList.ContainsKey(SceneManager.GetActiveScene().name)) {
			// if they have compare the new data to the old data, to see if they have got a better score.
			LevelData currentData = mostRecentSave.availableLevelList[SceneManager.GetActiveScene().name];
			// check if new values are better
			if (currentData.itemsCollected > newLevelData.itemsCollected) {
				newLevelData.itemsCollected = currentData.itemsCollected;
			}
			if (currentData.movesTaken < newLevelData.movesTaken) {
				newLevelData.movesTaken = currentData.movesTaken;
			}
			// Maintain item revealed state between completions.
			newLevelData.isItemRevealed = currentData.isItemRevealed;
			// Update the with the new level data.
			mostRecentSave.availableLevelList[SceneManager.GetActiveScene().name] = newLevelData;
		}
		else {
			// otherwise we just add the new level.
			mostRecentSave.availableLevelList.Add(SceneManager.GetActiveScene().name, newLevelData);
		}

		SaveProgressSystem.SaveProgress(mostRecentSave);
	}

	/// <summary>
	/// Toggles the visibilty of the collectable tooltip.
	/// TODO:: Currently just disable/enables the object, most likely change to animation.
	/// </summary>
	private void ToggleCollectableTooltip() {
		if (collectableTooltip.activeSelf) {
			collectableTooltip.SetActive(false);
		}
		else {
			collectableTooltip.SetActive(true);
		}
	}

	/// <summary>
	/// Ensures that on fake wins, the player score is horrible.	
	/// </summary>
	private void FakeWin() {
		moveCount_ = 100;
		collectedCount_ = -10;
	}
}
