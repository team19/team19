﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Simple utility to make it easier for Category Items to be set correctly in the Ui.
/// </summary>
public class UiCategoryItem : MonoBehaviour {

	[SerializeField]
	Text itemName = null;

	/// <summary>
	/// Sets up the Ui to match its assigned Category.
	/// 
	/// Sets Text, and makes it active.
	/// </summary>
	/// <param name="newCategory"> New Category to overide all settings with. </param>
	public void SetCategory(Category newCategory) {
		itemName.text = newCategory.categoryName;
		gameObject.SetActive(true);
	}
}
