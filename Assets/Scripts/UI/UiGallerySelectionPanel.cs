﻿using UnityEngine;

/// <summary>
/// Controls the Selection panel in the gallery.
/// Ensures it is populated based on mapInfo and resized properly.
/// </summary>
public class UiGallerySelectionPanel : MonoBehaviour {

	[SerializeField]
	MapInfo mapInfo = null;

	[SerializeField]
	UiGalleryItem templateGallerytem = null;

	[SerializeField]
	float levelSpacing = 240.0f;

	private bool isFirstItem_ = true;

	/// <summary>
	/// Populates the Content of the Gallery Selection panel and resizes the panel for all GalleryItem 's.
	/// </summary>
	private void Start() {
		float newHeight = 0;

		RectTransform levelItemRectTransform = templateGallerytem.GetComponent<RectTransform>();

		foreach (Category category in mapInfo.categories) {
			foreach (LevelInfo levelInfo in category.levelInfos) {
				GameObject newLevelItem = Instantiate(templateGallerytem.gameObject, transform);
				newLevelItem.GetComponent<UiGalleryItem>().PopulateItem(levelInfo);

				if (isFirstItem_) {
					isFirstItem_ = false;
					newLevelItem.GetComponent<UiGalleryItem>().Select();
				}

				// Position for next template
				levelItemRectTransform.localPosition = new Vector3(
					levelItemRectTransform.localPosition.x,
					levelItemRectTransform.localPosition.y - levelSpacing,
					levelItemRectTransform.localPosition.z);
				levelItemRectTransform.ForceUpdateRectTransforms();
				newHeight += levelSpacing;
			}
		}
		RectTransform rectTransform = GetComponent<RectTransform>();
		LittleLot.ScrollViewUtil.ResizeAndClampToTop(rectTransform, newHeight);
	}
}
