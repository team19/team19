﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Ui LevelItem, Selectable item for each Level available in the game.
/// </summary>
[RequireComponent(typeof(Button))]
public class UiLevelItem : MonoBehaviour {

	[SerializeField]
	Text itemName = null;

	[SerializeField]
	ScrollRect scrollRect = null;

	[SerializeField]
	RectTransform content = null;

	private RectTransform rectTransform_ = null;

	private Button button_ = null;

	private Animator buttonAnimator_ = null;

	private UiMapIcon mapIcon_ = null;

	private Button mapIconButton_ = null;

	private LevelInfo storedLevelInfo_ = null;

	private bool isLevelSelected_ = false;

	/// <summary>
	/// Ensures all the listeners are set. Also finds and connects the item to its MapIcon.
	/// </summary>
	private void OnEnable() {

		EventManagerLevelSelect.OnNewLevelItemSelected += TurnOff;

		if (!button_) {
			button_ = GetComponent<Button>();
		}
		button_.onClick.AddListener(LevelItemSelected);

		if (!buttonAnimator_) {
			buttonAnimator_ = GetComponent<Animator>();
		}

		if (!rectTransform_) {
			rectTransform_ = GetComponent<RectTransform>();
		}

		GameObject mapIconGameObject = GameObject.Find(storedLevelInfo_.displayName);

		if (mapIconGameObject) {
			mapIcon_ = mapIconGameObject.GetComponent<UiMapIcon>();
			mapIconButton_ = mapIcon_.GetComponent<Button>();
			mapIconButton_.onClick.AddListener(LevelItemSelected);
			mapIcon_.TurnOff();
		}
		else {
			Debug.LogWarning("No Map Icon Found " + storedLevelInfo_.displayName.ToString());
		}
	}

	/// <summary>
	/// Disables the toggle and removes the listeners from the toggle and the connected MapIcon.
	/// </summary>
	private void OnDisable() {
		EventManagerLevelSelect.OnNewLevelItemSelected -= TurnOff;
		button_.onClick.RemoveListener(LevelItemSelected);
		if (mapIconButton_) {
			mapIconButton_.onClick.AddListener(LevelItemSelected);
		}
	}

	/// <summary>
	/// Sets the Informaton on the item to the provided level info.
	/// Bases available information on the mostRecentSave so it only shows discovered and 
	/// relevent information. Item only shows completed levels, name and icon.
	/// </summary>
	/// <param name="newInfo"> The Level info to build the item from. </param>
	public void PopulateItem(LevelInfo newInfo) {

		storedLevelInfo_ = newInfo;

		gameObject.name = "Level:" + newInfo.displayName;


		SaveData mostRecentSave = SaveProgressSystem.RecentSave();

		if (mostRecentSave != null) {
			// Set Icon/Name
			if (mostRecentSave.availableLevelList.ContainsKey(newInfo.sceneNames[0])) {
				itemName.text = newInfo.displayName;
			}
			else {
				itemName.text = newInfo.hiddenName;
			}
		}
		gameObject.SetActive(true);
	}

	/// <summary>
	/// Turns the level item off and its connected MapIcon.
	/// </summary>
	public void TurnOff() {
		isLevelSelected_ = false;
		buttonAnimator_.SetBool("isSelected", false);
		buttonAnimator_.SetBool("Normal", true);
		if (mapIcon_) {
			mapIcon_.TurnOff();
		}
	}

	/// <summary>
	/// Generates Difficulty Select Information.
	/// When the item is selected it generates a new Difficulty Select.	
	/// Called when the items button or map icon is used.
	/// </summary>
	private void LevelItemSelected() {
		SoundBank.PlayButtonClick();
		LittleLot.ScrollViewUtil.CenterContentVerticallyOnRect(rectTransform_, scrollRect, ref content);
		if (!isLevelSelected_) {

			EventManagerLevelSelect.NewLevelItemSelected();
			buttonAnimator_.SetBool("isSelected", true);
			isLevelSelected_ = true;
			if (mapIcon_) {
				mapIcon_.TurnOn();
			}

			EventManagerLevelSelect.PopulateDifficultySelect(storedLevelInfo_);
			EventManagerLevelSelect.OpenDifficultySelect();

			LittleLot.ScrollViewUtil.CenterContentVerticallyOnRect(rectTransform_, scrollRect, ref content);

		}
	}


}
