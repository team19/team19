﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controls the opening and closing of the Gallery.
/// </summary>
public class UiGallery : MonoBehaviour {

	[SerializeField]
	Button openGallery = null;

	[SerializeField]
	Button closeGallery = null;

	[SerializeField]
	UiLightBulbNotification totalUnrevealedNotification = null;

	private Animator animator_ = null;
	
	/// <summary>
	/// Sets up buttons listeners.
	/// </summary>
	private void Start() {
		if (!animator_) {
			animator_ = GetComponent<Animator>();
		}

		openGallery.onClick.AddListener(OpenGallery);
		closeGallery.onClick.AddListener(CloseGallery);
		UpdateTotalUnrevealedNotification();
	}

	/// <summary>
	/// Removes button listeners.
	/// </summary>
	private void OnDestroy() {
		openGallery.onClick.RemoveListener(OpenGallery);
		closeGallery.onClick.RemoveListener(CloseGallery);
	}

	/// <summary>
	/// Opens the gallery UI, starting the animation.
	/// </summary>
	private void OpenGallery() {
		SoundBank.PlayButtonClick();
		animator_.SetBool("isInView", true);
	}

	/// <summary>
	/// Closes the gallery UI, starting the animation.
	/// </summary>
	private void CloseGallery() {
		SoundBank.PlayButtonClick();
		animator_.SetBool("isInView", false);
		UpdateTotalUnrevealedNotification();
	}
	
	/// <summary>
	/// Calculates the total number of unrevealed light bulb information.
	/// Updates the notification system for gallery button.
	/// </summary>
	private void UpdateTotalUnrevealedNotification() {
		int totalUnrevealed = 0;
		SaveData mostRecentSave = SaveProgressSystem.RecentSave();
		foreach (var item in mostRecentSave.availableLevelList) {
			if (!item.Value.isItemRevealed) {
				totalUnrevealed += item.Value.itemsCollected;
			}
		}
		totalUnrevealedNotification.UpdateNotificationText(totalUnrevealed);
	}
}
