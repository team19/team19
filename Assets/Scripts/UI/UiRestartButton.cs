﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using LittleLot;

/// <summary>
/// Restarts the current scene.
/// This class also ensures the restart button isn't visible during pause or win.
/// </summary>
public class UiRestartButton : MonoBehaviour {
	[SerializeField]
	Button restartButton = null;

	/// <summary>
	/// Adds listener to button, completion and pause menu.
	/// Ensures it dissapears when the pause menu opens,
	/// and reappears when the pause menu closes.
	/// </summary>
	private void Start() {
		restartButton.onClick.AddListener(RestartScene);
		EventManagerDesignObject.OnCompletion += DisableRestartButton;
		EventManagerPause.OnPauseMenuOpen += DisableRestartButton;
		EventManagerPause.OnPauseMenuClose += EnableRestartButton;
	}

	/// <summary>
	/// Removes all the listeners from the object.
	/// Ensures the listeners don't get called outside of the scene creating errrors.
	/// </summary>
	private void OnDestroy() {
		restartButton.onClick.RemoveListener(RestartScene);
		EventManagerDesignObject.OnCompletion -= DisableRestartButton;
		EventManagerPause.OnPauseMenuOpen -= DisableRestartButton;
		EventManagerPause.OnPauseMenuClose -= EnableRestartButton;
	}

	/// <summary>
	/// Reload the Current Scene Scene.
	/// </summary>
	public void RestartScene() {
		SoundBank.PlayButtonClick();
		restartButton.interactable = false;
		LoadingTransitionController.AnimatedLoadSceneAsync(SceneManager.GetActiveScene().name);
	}

	/// <summary>
	/// Disables the Restart Button making it non-interactable.	
	/// </summary>
	private void DisableRestartButton() {
		restartButton.interactable = false;
	}

	/// <summary>
	/// Enables the Restart Button making it non-interactable.	
	/// </summary>
	private void EnableRestartButton() {
		restartButton.interactable = true;
	}
}
