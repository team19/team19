﻿using UnityEngine;
using UnityEngine.UI;

using LittleLot;

/// <summary>
/// Handles Opening and closing of about page.
/// </summary>
[RequireComponent(typeof(Animator))]
public class UiAboutPage : MonoBehaviour {

	[SerializeField]
	Button openAboutPageButton = null;

	[SerializeField]
	Button closeAboutPageButton = null;

	[SerializeField]
	Button replayTutorialButton = null;

	private Animator animator_ = null;

	/// <summary>
	/// Ensure event listerners are added, and make sure the object is not active in the scene.
	/// </summary>
	private void Start() {
		openAboutPageButton.onClick.AddListener(OpenAboutPage);
		closeAboutPageButton.onClick.AddListener(CloseAboutPage);
		replayTutorialButton.onClick.AddListener(ReplayTutorial);
		// Makes sure the animator is set.
		if (!animator_) {
			animator_ = GetComponent<Animator>();
		}
	}

	/// <summary>
	/// Make Sure all Event Listeners have been removed when destroying.
	/// </summary>
	private void OnDestroy() {
		openAboutPageButton.onClick.RemoveListener(OpenAboutPage);
		closeAboutPageButton.onClick.RemoveListener(CloseAboutPage);
		replayTutorialButton.onClick.RemoveListener(ReplayTutorial);
	}

	/// <summary>
	/// Start the animation to bring the about page into screen view.	
	/// </summary>
	private void OpenAboutPage() {
		SoundBank.PlayButtonClick();
		animator_.SetBool("isInView", true);
	}

	/// <summary>
	/// Start the animation to set the about page out of screen view.	
	/// </summary>
	private void CloseAboutPage() {
		SoundBank.PlayButtonClick();
		animator_.SetBool("isInView", false);
	}

	/// <summary>
	/// Reloads and restarts the tutorial sequence.
	/// </summary>
	private void ReplayTutorial() {
		SoundBank.PlayButtonClick();
		LoadingTransitionController.AnimatedLoadSceneAsync("OnBoardingSequence", "AnimatedLoadingScreen");
		SaveData mostRecentSave = SaveProgressSystem.RecentSave();
		mostRecentSave.finishedOnboarding = false;
		SaveProgressSystem.SaveProgress(mostRecentSave);
	}
}
