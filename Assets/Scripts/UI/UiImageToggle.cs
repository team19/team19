﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Simple utility class for handling toggleable images.
/// One on and one off Image.
/// </summary>
public class UiImageToggle : MonoBehaviour {

	[SerializeField]
	Image onImage = null;

	[SerializeField]
	Image offImage = null;

	/// <summary>
	/// Changes to the offImage only being enabled.
	/// </summary>
	public void TurnOff() {
		onImage.enabled = false;
		offImage.enabled = true;
	}

	/// <summary>
	/// Changes to the onImage only being enabled.
	/// </summary>
	public void TurnOn() {
		onImage.enabled = true;
		offImage.enabled = false;
	}
}
