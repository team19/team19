﻿using UnityEngine;
using UnityEngine.UI;

using LittleLot;
using System;

/// <summary>
/// Simple Debug menu that show when 5 fingers held down, or F12 pressed.
/// Only possible in debug mode.
/// 
/// Allows reloading of the map scene.
/// Showing Save Data information in the log, and deleting save data.
/// Allows Fake win conditions to be fired in various scenes.
/// It has a prefab panel, that sets up a bunch of UI elements to make it easier to use.
/// </summary>
public class UiDebugPanel : MonoBehaviour {

	[SerializeField]
	Text debugTitle = null;

	[SerializeField]
	Button restartGame = null;

	[SerializeField]
	Button reloadScene = null;

	[SerializeField]
	Button logSaveData = null;

	[SerializeField]
	Button deleteSave = null;

	[SerializeField]
	Button deletePlayerPrefs = null;

	[SerializeField]
	Button fakeWin = null;

	[SerializeField]
	Button closeDebugPanel = null;

	[SerializeField]
	GitInformation gitInformation = null;

	[SerializeField]
	Text displayGitCommit = null;
	
	private Animator animator_ = null;

	/// <summary>
	/// Adds the listeners to the buttons, will disable itself if the DebugUi is Disabled.
	/// </summary>
	private void Start() {
		if (!DebugUi.IsEnabled()) {
			gameObject.SetActive(false);
			Destroy(this);
			return;
		}
		if (!animator_) {
			animator_ = GetComponent<Animator>();
		}
		animator_.SetBool("isInView", false);

		restartGame.onClick.AddListener(RestartGame);
		restartGame.onClick.AddListener(SaveProgressSystem.DeleteSave);

		reloadScene.onClick.AddListener(ReloadScene);

		logSaveData.onClick.AddListener(SaveProgressSystem.LogAvailableSaveData);

		deleteSave.onClick.AddListener(ReloadScene);
		deleteSave.onClick.AddListener(SaveProgressSystem.DeleteSave);

		deletePlayerPrefs.onClick.AddListener(DeletePlayerPrefrences);

		fakeWin.onClick.AddListener(FakeWin);

		closeDebugPanel.onClick.AddListener(CloseDebugPanel);

		EventManagerLoadingSystem.OnLoadingFinished += MatchTitleToScene;

		displayGitCommit.text = string.Format("Version: {1}{0}Branch: {2}{0}Commit: {3}{0}",
											  Environment.NewLine,
											  Application.version,
											  gitInformation.Branch,
											  gitInformation.CommitHash);

		// Make sure setup finishes when there is no loading system (i.e. ran in editor from scene)
		if (!LoadingTransitionController.HasLoadingSystem()) {
			MatchTitleToScene();
		}
	}

	/// <summary>
	/// Make Sure all Event Listeners have been removed when destroying.
	/// </summary>
	private void OnDestroy() {
		restartGame.onClick.RemoveListener(RestartGame);
		restartGame.onClick.RemoveListener(SaveProgressSystem.DeleteSave);

		reloadScene.onClick.RemoveListener(ReloadScene);

		logSaveData.onClick.RemoveListener(SaveProgressSystem.LogAvailableSaveData);

		deleteSave.onClick.RemoveListener(ReloadScene);
		deleteSave.onClick.RemoveListener(SaveProgressSystem.DeleteSave);

		deletePlayerPrefs.onClick.RemoveListener(DeletePlayerPrefrences);

		fakeWin.onClick.AddListener(FakeWin);

		closeDebugPanel.onClick.RemoveListener(CloseDebugPanel);

		EventManagerLoadingSystem.OnLoadingFinished -= MatchTitleToScene;
	}

	/// <summary>
	/// When 5 fingers or f12 pressed, the debug panel becomes visible.
	/// </summary>
	private void Update() {
		if ((Input.touchCount >= 5 || Input.GetKeyDown(KeyCode.F12) && !animator_.GetBool("isInView"))) {
			animator_.SetBool("isInView", true);
			deletePlayerPrefs.GetComponentInChildren<Text>().text = "Delete PlayerPrefs";
			EventManagerDebug.OpenDebugPanel();
		}
	}

	/// <summary>
	/// Restart the game from the start.
	/// </summary>
	private void RestartGame() {
		CloseDebugPanel();
		LoadingTransitionController.AnimatedLoadSceneAsync("OnBoardingSequence", "AnimatedLoadingScreen");
	}

	/// <summary>
	/// Reload the current Scene.
	/// </summary>
	private void ReloadScene() {
		CloseDebugPanel();
		LoadingTransitionController.AnimatedLoadSceneAsync(LoadingTransitionController.GetActiveScene(), "AnimatedLoadingScreen");
	}

	/// <summary>
	/// Close the debug panel.
	/// </summary>
	private void CloseDebugPanel() {
		animator_.SetBool("isInView", false);
		EventManagerDebug.CloseDebugPanel();
	}

	/// <summary>
	/// Launches an event to simulate a win, doesn't actually complete the object.
	/// </summary>
	private void FakeWin() {
		CloseDebugPanel();
		EventManagerDebug.FakeWin();
		EventManagerDesignObject.DesignObjectCompleted();
	}

	/// <summary>
	/// Matches the debug Ui title to the current scene name.
	/// </summary>
	private void MatchTitleToScene() {
		debugTitle.text = "SceneName:\n" + LoadingTransitionController.GetActiveScene();
	}

	/// <summary>
	/// Deletes all the players player prefrences in PlayerPrefs.
	/// </summary>
	private void DeletePlayerPrefrences() {
		deletePlayerPrefs.GetComponentInChildren<Text>().text = "Deleted";
		PlayerPrefs.DeleteAll();
	}
}
