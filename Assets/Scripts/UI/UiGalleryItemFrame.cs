﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controls the Information within the gallery iteself.
/// </summary>
public class UiGalleryItemFrame : MonoBehaviour {
	[SerializeField]
	Image galleryRender = null;

	[SerializeField]
	Text title = null;

	[SerializeField]
	Text designerName = null;

	[SerializeField]
	UiGalleryLightBulbInformation[] lightBulbInformationPanels = new UiGalleryLightBulbInformation[3];

	private Animator animator_ = null;

	/// <summary>
	/// Adds listener for new Item Frames.
	/// </summary>
	private void OnEnable() {
		EventManagerGallery.OnPopulateItemFrame += PopulateItemFrame;
		// Makes sure the animator is set.
		if (!animator_) {
			animator_ = GetComponent<Animator>();
		}
	}

	/// <summary>
	/// Removes listener for new frames
	/// </summary>
	private void OnDisable() {
		EventManagerGallery.OnPopulateItemFrame -= PopulateItemFrame;
	}

	/// <summary>
	/// Populates the Item frame in the gallery UI with the provided LevelInfo.
	/// </summary>
	/// <param name="levelInfo"></param>
	private void PopulateItemFrame(LevelInfo levelInfo) {

		SaveData mostRecentSave = SaveProgressSystem.RecentSave();

		// Check if the have discovered this level yet.
		if (mostRecentSave.availableLevelList.ContainsKey(levelInfo.sceneNames[0])) {
			title.text = levelInfo.displayName;
			designerName.text = levelInfo.designerName;
			galleryRender.sprite = levelInfo.galleryRenderLit;
		}
		else {
			title.text = levelInfo.hiddenName;
			designerName.text = "";
			galleryRender.sprite = levelInfo.galleryRenderUnlit;
		}

		int lightBulbIndex = 0;
		foreach (var item in lightBulbInformationPanels) {
			// Sets text and lock the item.
			item.Lock();
			item.SetLightBulbInformation(levelInfo, lightBulbIndex);

			// If the player has attempted this level.
			if (mostRecentSave.availableLevelList.ContainsKey(levelInfo.sceneNames[lightBulbIndex])) {
				// get the level data.
				LevelData levelData = mostRecentSave.availableLevelList[levelInfo.sceneNames[lightBulbIndex]];

				// check if they have collected the lightbulb.
				if (levelData.itemsCollected > 0) {
					item.Unlock();
					// check if they have revealed it before.
					if (levelData.isItemRevealed) {
						item.ShowInformation();
					}

				}
			}
			lightBulbIndex++;
		}

		animator_.Play("spawnGalleryInfo", 0, 0);
	}
}
