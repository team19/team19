﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Converts a Unity Text or 3D Text element into a curved Text.
/// 
/// Manipulated by the Curve graph within the Unity Editor.
/// </summary>
[RequireComponent(typeof(RectTransform))]
public class CurvedText : BaseMeshEffect {

	[SerializeField]
	AnimationCurve curveForText = AnimationCurve.Linear (0, 0, 1, 10);

	[SerializeField]
	float curveMultiplier = 1;

	private RectTransform rectTransform;

#if UNITY_EDITOR
	/// <summary>
	/// Updates the Ui whenever the editor detects a change.
	/// Causes compile errors at runtime, for non-editor platforms, as no overrideable function outside of editor. 
	/// </summary>
	protected override void OnValidate() {
		base.OnValidate();
		if (curveForText[0].time != 0) {
			var tmpRect = curveForText[0];
			tmpRect.time = 0;
			curveForText.MoveKey(0, tmpRect);
		}
		if (!rectTransform) {
			rectTransform = GetComponent<RectTransform>();
		}
		if (curveForText[curveForText.length - 1].time != rectTransform.rect.width) {
			OnRectTransformDimensionsChange();
		}
	}
#endif

	/// <summary>
	/// Forces the Curved text to update when enabled.
	/// </summary>
	protected override void OnEnable() {
		base.OnEnable();
		if (!rectTransform) {
			rectTransform = GetComponent<RectTransform>();
		}
		OnRectTransformDimensionsChange();
	}

	/// <summary>
	/// Overrides the base ModifyMesh function used by 3D Text.
	/// </summary>
	public override void ModifyMesh(Mesh mesh) {
		if (!IsActive())
			return;

		List<UIVertex> list = new List<UIVertex>();
		using (VertexHelper vertexHelper = new VertexHelper(mesh)) {
			vertexHelper.GetUIVertexStream(list);
		}

		ModifyVertices(list);

		using (VertexHelper vertexHelper2 = new VertexHelper()) {
			vertexHelper2.AddUIVertexTriangleStream(list);
			vertexHelper2.FillMesh(mesh);
		}
	}

	/// <summary>
	/// Overrides the base ModifyMesh function used by Text.
	/// </summary>	
	public override void ModifyMesh(VertexHelper vh) {
		if (!IsActive())
			return;

		List<UIVertex> vertexList = new List<UIVertex>();
		vh.GetUIVertexStream(vertexList);

		ModifyVertices(vertexList);

		vh.Clear();
		vh.AddUIVertexTriangleStream(vertexList);
	}

	/// <summary>
	/// Overrides the base ModifyVertices function used by both types of Text.
	/// 
	/// Updates the vertex position to match curve points.
	/// </summary>	
	public void ModifyVertices(List<UIVertex> verts) {
		if (!IsActive())
			return;

		for (int index = 0; index < verts.Count; index++) {
			var uiVertex = verts[index];
			uiVertex.position.y += curveForText.Evaluate(rectTransform.rect.width * rectTransform.pivot.x + uiVertex.position.x) * curveMultiplier;
			verts[index] = uiVertex;
		}
	}

	/// <summary>
	/// When ever the connected rectTransform changes this is called.
	/// 
	/// It will ensure the curve is updated to match.
	/// </summary>
	protected override void OnRectTransformDimensionsChange() {
		if (rectTransform) {
			var tmpRect = curveForText[curveForText.length - 1];
			tmpRect.time = rectTransform.rect.width;
			curveForText.MoveKey(curveForText.length - 1, tmpRect);
		}
	}
}