﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Ensures correct image is shown on audio button, alows turning off and on of different audio settings.
/// </summary>
[RequireComponent(typeof(Button))]
public class UiToggleAudio : MonoBehaviour {
	enum ToggleType { kSfx, kMusic };

	[SerializeField]
	UiImageToggle audioIconToggle = null;

	[SerializeField]
	ToggleType audioType = ToggleType.kMusic;

	bool isOn_ = false;

	Button toggleAudioButton_ = null;

	/// <summary>
	/// Enable button listener to correct function, and check Sound manager for audio settings.
	/// Ensures correct image is showing on load.
	/// </summary>
	private void Start() {
		if (!toggleAudioButton_) {
			toggleAudioButton_ = GetComponent<Button>();
		}
		// Check Current player prefs to find out audio state on or off.
		switch (audioType) {
			case ToggleType.kSfx:
				toggleAudioButton_.onClick.AddListener(ToggleSfx);
				if (SoundManager.IsSfxEnabled()) {
					isOn_ = true;
				}
				else {
					isOn_ = false;
				}
				break;
			case ToggleType.kMusic:
				toggleAudioButton_.onClick.AddListener(ToggleMusic);
				if (SoundManager.IsMusicEnabled()) {
					isOn_ = true;
				}
				else {
					isOn_ = false;
				}
				break;
			default:
				break;
		}

		if (isOn_) {
			audioIconToggle.TurnOn();
		}
		else {
			audioIconToggle.TurnOff();
		}
	}

	/// <summary>
	/// Ensure listners are removed.
	/// </summary>
	private void OnDestroy() {
		switch (audioType) {
			case ToggleType.kSfx:
				toggleAudioButton_.onClick.RemoveListener(ToggleSfx);
				break;
			case ToggleType.kMusic:

				toggleAudioButton_.onClick.RemoveListener(ToggleMusic);
				break;
			default:
				break;
		}
	}

	/// <summary>
	/// Swaps Images based on wether it is on or not.
	/// Also turns Music audio on or off based on state.
	/// </summary>
	private void ToggleMusic() {
		SoundBank.PlayButtonClick();
		if (isOn_) {
			isOn_ = false;
			audioIconToggle.TurnOff();
			SoundManager.DisableMusic();
		}
		else {
			isOn_ = true;
			audioIconToggle.TurnOn();
			SoundManager.EnableMusic();
			SoundManager.PlayPreviousMusic();
		}
	}

	/// <summary>
	/// Swaps Images based on wether it is on or not.
	/// Also turns Sfx audio on or off based on state.
	/// </summary>
	private void ToggleSfx() {
		SoundBank.PlayButtonClick();
		if (isOn_) {
			isOn_ = false;
			audioIconToggle.TurnOff();
			SoundManager.DisableSfx();
		}
		else {
			isOn_ = true;
			audioIconToggle.TurnOn();
			SoundManager.EnableSfx();
		}
	}
}
