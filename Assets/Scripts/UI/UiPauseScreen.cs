﻿using UnityEngine;
using UnityEngine.UI;

using LittleLot;

/// <summary>
/// Pause screen for during game play.
/// Allows the user to go back to the Map screen, and change audio settings.
/// </summary>
[RequireComponent(typeof(Animator))]
public class UiPauseScreen : MonoBehaviour {

	[SerializeField]
	Button continueButton = null;

	[SerializeField]
	Button mapButton = null;

	[SerializeField]
	Button retryButton = null;

	private Animator panelAnimator_ = null;

	/// <summary>
	/// Gets the refrence to the animator for use later.
	/// </summary>
	private void Start() {
		if (!panelAnimator_) {
			panelAnimator_ = GetComponent<Animator>();
		}
	}

	/// <summary>
	/// Ensures all the buttons listeners are set.
	/// As well as listeners for the Opening and closing of the Menu are set.
	/// </summary>
	private void OnEnable() {
		continueButton.onClick.AddListener(EventManagerPause.ClosePauseMenu);
		continueButton.onClick.AddListener(SoundBank.PlayButtonClick);
		mapButton.onClick.AddListener(QuitToMapScreen);
		retryButton.onClick.AddListener(ReloadCurrentScene);

		EventManagerPause.OnPauseMenuOpen += OpenPauseMenu;
		EventManagerPause.OnPauseMenuClose += ClosePauseMenu;
	}

	/// <summary>
	/// Removes all the listeners.
	/// </summary>
	private void OnDisable() {
		continueButton.onClick.RemoveListener(EventManagerPause.ClosePauseMenu);
		continueButton.onClick.RemoveListener(SoundBank.PlayButtonClick);
		mapButton.onClick.RemoveListener(QuitToMapScreen);
		retryButton.onClick.RemoveListener(ReloadCurrentScene);

		EventManagerPause.OnPauseMenuOpen -= OpenPauseMenu;
		EventManagerPause.OnPauseMenuClose -= ClosePauseMenu;
	}

	/// <summary>
	/// Loads the map screen when quit is clicked.
	/// Also sets the button text to loading.
	/// </summary>
	private void QuitToMapScreen() {
		SoundBank.PlayButtonClick();
		continueButton.interactable = false;
		mapButton.interactable = false;
		LoadingTransitionController.AnimatedLoadSceneAsync("MapScreen", "AnimatedLoadingScreen");
	}

	/// <summary>
	/// Loads the map screen when quit is clicked.
	/// Also sets the button text to loading.
	/// </summary>
	private void ReloadCurrentScene() {
		SoundBank.PlayButtonClick();
		continueButton.interactable = false;
		mapButton.interactable = false;
		LoadingTransitionController.AnimatedLoadSceneAsync(LoadingTransitionController.GetActiveScene(), "AnimatedLoadingScreen");
	}

	/// <summary>
	/// Start the animation to move the PauseScreen into screen view.
	/// If it is already in view it will not play again.
	/// </summary>
	private void OpenPauseMenu() {
		panelAnimator_.SetBool("isInView", true);
	}

	/// <summary>
	/// Start the animation to move the PauseScreen out of screen view.
	/// If it is already out of view it will not play again.
	/// </summary>
	private void ClosePauseMenu() {
		panelAnimator_.SetBool("isInView", false);
	}
}
