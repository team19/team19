﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Gallery Item in scroll view.
/// Setup listeners and allows itself to be populated, and maintains the items notifications.
/// </summary>
public class UiGalleryItem : MonoBehaviour {

	[SerializeField]
	Image itemPicture = null;

	[SerializeField]
	ScrollRect scrollRect = null;

	[SerializeField]
	RectTransform content = null;

	[SerializeField]
	UiLightBulbNotification itemUnrevealedNotification = null;

	private RectTransform rectTransform_ = null;

	private Button button_ = null;

	private Animator buttonAnimator_ = null;

	private LevelInfo storedLevelInfo_ = null;

	private bool isItemSelected = false;

	/// <summary>
	/// Adds button listeners and item events.
	/// </summary>
	private void OnEnable() {
		EventManagerGallery.OnNewGalleryItemSelected += TurnOff;

		if (!button_) {
			button_ = GetComponent<Button>();
		}
		button_.onClick.AddListener(GalleryItemSelected);

		if (!buttonAnimator_) {
			buttonAnimator_ = GetComponent<Animator>();
		}

		if (!rectTransform_) {
			rectTransform_ = GetComponent<RectTransform>();
		}
	}

	/// <summary>
	/// Removes button listeners and item events.
	/// </summary>
	private void OnDisable() {
		EventManagerGallery.OnNewGalleryItemSelected -= TurnOff;
		EventManagerGallery.OnLightBulbInfoRevealed -= UpdateUnrevealedNotification;

		if (!button_) {
			button_ = GetComponent<Button>();
		}
		button_.onClick.RemoveListener(GalleryItemSelected);
	}

	/// <summary>
	/// Populates the item with the provided level info.
	/// </summary>
	/// <param name="newlevelInfo"></param>
	public void PopulateItem(LevelInfo newlevelInfo) {
		storedLevelInfo_ = newlevelInfo;

		itemPicture.sprite = storedLevelInfo_.galleryIcon;
		gameObject.SetActive(true);
		UpdateUnrevealedNotification();
	}

	/// <summary>
	/// Set this item as the selected gallery item in the gallery inner frame.
	/// Populates then Repositions the scroll view to bring it into view fully.
	/// </summary>
	public void Select() {
		EventManagerGallery.NewGalleryItemSelected();
		buttonAnimator_.SetBool("isSelected", true);
		isItemSelected = true;
		EventManagerGallery.PopulateItemFrame(storedLevelInfo_);

		LittleLot.ScrollViewUtil.CenterContentVerticallyOnRect(rectTransform_, scrollRect, ref content);
		// In focus so could be update so should listen to this.
		EventManagerGallery.OnLightBulbInfoRevealed += UpdateUnrevealedNotification;
	}

	/// <summary>
	/// Sets the item frame to this item start animations etc, unless it is already selected.
	/// </summary>
	private void GalleryItemSelected() {
		SoundBank.PlayButtonClick();
		if (!isItemSelected) {
			Select();
		}
	}

	/// <summary>
	/// Turns it off removing focus and allowing it to be reselected later.
	/// This is used to allow the currently used item to keep focus and be removed when another is selected.
	/// </summary>
	private void TurnOff() {
		isItemSelected = false;
		buttonAnimator_.SetBool("isSelected", false);
		buttonAnimator_.SetBool("Normal", true);

		// Doesn't need to listen for this as its not currently in focus.
		EventManagerGallery.OnLightBulbInfoRevealed -= UpdateUnrevealedNotification;
	}

	/// <summary>
	/// Calculates the number of unrevealed light bulb information for this item.
	/// Updates the notificationsystem for this frame.
	/// </summary>
	private void UpdateUnrevealedNotification() {

		int itemUnrevealedLightBulbs = 0;

		SaveData mostRecentSave = SaveProgressSystem.RecentSave();
		foreach (string sceneName in storedLevelInfo_.sceneNames) {
			foreach (var completedLevel in mostRecentSave.availableLevelList) {
				// if it is one of this items levels.
				if (completedLevel.Key == sceneName) {
					// if they havnt already revealed the item.
					if (!completedLevel.Value.isItemRevealed) {
						// add the number of items collected ( will be 0 if they dont have it)
						itemUnrevealedLightBulbs += completedLevel.Value.itemsCollected;
					}
				}
			}
		}
		
		itemUnrevealedNotification.UpdateNotificationText(itemUnrevealedLightBulbs);
	}
}
