﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Simply ensures the version number displays correctly in bottom right of screen.
/// </summary>
public class UiDisplayVersion : MonoBehaviour {

	[Tooltip("Forces the Version Number to display, even if DebugUi is disabled. ")]
	[SerializeField]
	bool isPersistant =false;

	[SerializeField]
	Text displayText = null;

	/// <summary>
	/// Sets the child text component to display the game version number, 
	/// will disable itself if the DebugUi is Disabled.	
	/// </summary>
	void Start() {
		if (!DebugUi.IsEnabled() && !isPersistant) {
			gameObject.SetActive(false);
			Destroy(this);
			return;
		}
		displayText.text = Application.version;
	}

	/// <summary>
	/// Ensures the value shownn in the Editor view is the current version.
	/// </summary>
	private void OnValidate() {
		if (displayText) {
			displayText.text = Application.version;
		}
	}
}
