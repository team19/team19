﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controls the revealing and hidding of The Galleries Light Bulb information panel.
/// </summary>
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Button))]
public class UiGalleryLightBulbInformation : MonoBehaviour {

	[SerializeField]
	Text description = null;

	private Button button_ = null;

	private Animator animator_ = null;

	private LevelInfo levelInfo_ =null;

	private int lightBulbIndex_ = 0;

	private bool isRevealed_ = false;

	/// <summary>
	/// Adds the listener to the button.
	/// </summary>
	private void OnEnable() {
		if (!animator_) {
			animator_ = GetComponent<Animator>();
		}

		if (!button_) {
			button_ = GetComponent<Button>();
		}
		button_.onClick.AddListener(RevealInformation);
	}

	/// <summary>
	/// Removes the buttons listener.
	/// </summary>
	private void OnDisable() {
		button_.onClick.RemoveListener(RevealInformation);
	}

	/// <summary>
	/// Sets the light bulb information text, and stores the variables for use in saving.
	/// </summary>
	/// <param name="levelInfo"> Used to find light bulb information. </param>
	/// <param name="lightBulbIndex"> The ligh bulb index in the levelInfo a value of 0-2.</param>
	public void SetLightBulbInformation(LevelInfo levelInfo, int lightBulbIndex) {
		description.text = levelInfo.lightBulbInfo[lightBulbIndex];
		levelInfo_ = levelInfo;
		lightBulbIndex_ = lightBulbIndex;
	}

	/// <summary>
	/// Disables interaction with LightBulb information button.
	/// Showing the disabled animation.
	/// </summary>
	public void Lock() {
		animator_.Play("Disabled", 0, 0);
		animator_.SetBool("isSelected", false);
		button_.interactable = false;
		isRevealed_ = false;
	}

	/// <summary>
	/// Enables interaction with LightBulb information button.
	/// Showing the normal idle animation.
	/// </summary>
	public void Unlock() {
		animator_.Play("Normal", 0, 0);
		button_.interactable = true;
	}

	/// <summary>
	/// Shows the light bulb information and sets it to the correct animation.
	/// Showing the pressed/revealed animation.
	/// </summary>
	public void ShowInformation() {
		animator_.Play("Pressed", 0, 0);
		animator_.SetBool("isSelected", true);
		isRevealed_ = true;
		
	}

	/// <summary>
	/// Reveals the information and saves that it has been revealed to the save data.
	/// </summary>
	private void RevealInformation() {
		if (!isRevealed_) {
			SoundBank.PlayReveal();
			ShowInformation();			
			if (levelInfo_) {
				// get the most recent save data.
				SaveData mostRecentSave = SaveProgressSystem.RecentSave();
				// get the current level data.
				LevelData currentData = mostRecentSave.availableLevelList[levelInfo_.sceneNames[lightBulbIndex_]];
				// update the items revealed status.
				currentData.isItemRevealed = true;
				// update the level data.
				mostRecentSave.availableLevelList[levelInfo_.sceneNames[lightBulbIndex_]] = currentData;
				// Save the new level data.
				SaveProgressSystem.SaveProgress(mostRecentSave);
			}
			// Update the reveal info notification after the data has been saved.
			EventManagerGallery.LightBulbInfoRevealed();
		}
	}
}
