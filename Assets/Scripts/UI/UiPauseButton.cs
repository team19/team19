﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Brings up the Pause menu in the game.
/// UiPauseScreen is brought up when this buton is clicked.
/// </summary>
[RequireComponent(typeof(Button))]
public class UiPauseButton : MonoBehaviour {

	private Button pauseButton_ =null;

	/// <summary>
	/// Adds the onclick listener to the button.
	/// </summary>
	private void Start() {
		if (!pauseButton_) {
			pauseButton_ = GetComponent<Button>();
		}
		pauseButton_.onClick.AddListener(OpenPauseMenu);
	}

	/// <summary>
	/// Ensures the pause menu event listener is assigned.
	/// </summary>
	private void OnEnable() {
		EventManagerPause.OnPauseMenuClose += OnPauseMenuClosed;
		EventManagerDesignObject.OnCompletion += OnGameCompleted;
	}

	/// <summary>
	/// Removes the Event listeners.
	/// </summary>
	private void OnDisable() {
		EventManagerPause.OnPauseMenuClose -= OnPauseMenuClosed;
		EventManagerDesignObject.OnCompletion -= OnGameCompleted;
	}

	/// <summary>
	/// When the button is clicked it opens the pause menu.
	/// Sets itself inactive until the pause menu closes.
	/// </summary>
	private void OpenPauseMenu() {
		SoundBank.PlayButtonClick();
		EventManagerPause.OpenPauseMenu();
		pauseButton_.interactable = false;
	}

	/// <summary>
	/// Reactives the pause button when the pause menu closes.
	/// </summary>
	private void OnPauseMenuClosed() {
		pauseButton_.interactable = true;
	}

	/// <summary>
	/// Disables the pause button when the win screen or lose screen are up.
	/// Stops the ability to change and edit things whilst it is saving etc.
	/// </summary>
	private void OnGameCompleted() {
		pauseButton_.interactable = false;
	}
}
