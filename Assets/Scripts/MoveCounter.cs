﻿using System.Collections;

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles the move counter and countdown of moves on completion.
/// </summary>
[RequireComponent(typeof(Text))]
public class MoveCounter : MonoBehaviour {

	private float countDownTime_ = 2.4f;

	private Text displayText_ = null;

	private int moveCount_ =0;

	/// <summary>
	/// Makes the text display the availableMoves.
	/// </summary>
	private void Start() {
		if (!displayText_) {
			displayText_ = GetComponent<Text>();
		}
		displayText_.text = AvailableMoveText();
	}

	/// <summary>
	/// Adds the listeners for decrease/increase available moves and camera in position.
	/// </summary>
	private void OnEnable() {
		EventManagerMoveCount.OnMoveCountDecrease += DecreaseMove;
		EventManagerMoveCount.OnMoveCountIncrease += IncreaseMove;

		EventManagerWinScreen.OnCameraInPos += StartCountDown;
	}

	/// <summary>
	/// Removes the listeners for decrease/increase available moves and camera in position.
	/// </summary>
	private void OnDisable() {
		EventManagerMoveCount.OnMoveCountDecrease -= DecreaseMove;
		EventManagerMoveCount.OnMoveCountIncrease -= IncreaseMove;

		EventManagerWinScreen.OnCameraInPos -= StartCountDown;
	}

	/// <summary>
	/// Ensures the value shownn in the Editor view is the availableMoves value.
	/// </summary>
	private void OnValidate() {
		if (displayText_) {
			displayText_.text = AvailableMoveText();
		}
	}

	/// <summary>
	/// Decreases the number of available moves, by the provided decreaseAmount.
	/// </summary>
	private void DecreaseMove() {
		moveCount_--;
		displayText_.text = AvailableMoveText();
	}

	/// <summary>
	/// Increases the number of available moves, by the provided increaseAmount.
	/// </summary>
	private void IncreaseMove() {
		moveCount_++;
		displayText_.text = AvailableMoveText();
	}

	/// <summary>
	/// Determines the text for the number of moves available to use.
	/// </summary>
	/// <returns> Formated string of current move count. </returns>
	private string AvailableMoveText() {
		return string.Format("{0,4}", moveCount_.ToString());
	}

	/// <summary>
	/// Start coroutine that counts down the moves on the move counter.	
	/// </summary>
	private void StartCountDown() {
		StartCoroutine(CountDownMoves());
	}

	/// <summary>
	/// Counts the moves down over time with countDownDelay_ between each decrease.
	/// Notifies when the countdown is finished.
	/// </summary>
	private IEnumerator CountDownMoves() {
		// calculate countdown rate
		float countDownDelay = countDownTime_ / (moveCount_ + 1);
		float pitchChange = 0.08f;
		float pitch = 0.8f;
		EventManagerWinScreen.TotalMoveCount(moveCount_);
		// Count down all the moves to zero
		while (moveCount_ > 0) {
			moveCount_--;
			displayText_.text = AvailableMoveText();
			EventManagerWinScreen.CountDownMove();
			SoundBank.PlayDescendingCountDown(pitch);
			pitch += pitchChange;
			// wait for a few seconds before counting down again.
			yield return new WaitForSeconds(countDownDelay);
		}
		EventManagerWinScreen.MoveCountDownFinished();
	}
}
