﻿using UnityEngine;

/// <summary>
/// Simple Utility Class that can be used to determine if DebugUi Should be displayed.
/// </summary>
public class DebugUi : MonoBehaviour {
	
	/// <summary>
	/// Returns true if the Debug Ui should be enabled.
	/// </summary>
	/// <returns> True if it should be enabled. </returns>
	public static bool IsEnabled() {
#if DEBUGUIENABLED
		return true;
#else
		return false;
#endif
	}

}
