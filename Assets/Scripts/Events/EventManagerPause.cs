﻿using UnityEngine;

/// <summary>
/// Handles events to do with pause menu.
/// </summary>
public class EventManagerPause : MonoBehaviour {

	public delegate void EventHandlerPauseMenu();
	/// <summary>
	/// Event to listen to for when the pause menu should open.
	/// </summary>
	public static event EventHandlerPauseMenu OnPauseMenuOpen;

	/// <summary>
	/// Event to listen to for when the pause menu should open.
	/// </summary>
	public static event EventHandlerPauseMenu OnPauseMenuClose;

	/// <summary>
	/// Should be called when the pause menu needs to open.
	/// </summary>
	public static void OpenPauseMenu() {
		// notify all listeners to event.
		if (OnPauseMenuOpen != null) {
			OnPauseMenuOpen();
		}
	}

	/// <summary>
	/// Should be called when the pause menu needs to close.
	/// </summary>
	public static void ClosePauseMenu() {
		// notify all listeners to event.
		if (OnPauseMenuClose != null) {
			OnPauseMenuClose();
		}
	}
}
