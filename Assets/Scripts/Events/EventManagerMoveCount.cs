﻿using UnityEngine;

/// <summary>
/// Handles events to do with movement counting.
/// </summary>
public class EventManagerMoveCount : MonoBehaviour {

	public delegate void EventHandlerCountMove();
	/// <summary>	
	/// Event to listen to for when available moves increases.
	/// Takes the number of moves to increase by as parameter.
	/// </summary>
	public static event EventHandlerCountMove OnMoveCountIncrease;

	/// <summary>	
	/// Event to listen to for when available moves decreases.
	/// Takes the number of moves to decrease by as parameter.
	/// </summary>
	public static event EventHandlerCountMove OnMoveCountDecrease;

	/// <summary>
	/// Should be called to increase the available move count.    
	/// </summary>
	public static void IncreaseMoveCount() {
		// notify all listeners to event.
		if (OnMoveCountIncrease != null) {
			OnMoveCountIncrease();
		}
	}

	/// <summary>
	/// Should be called to decrease the available move count.    
	/// </summary>
	public static void DecreaseMoveCount() {
		// notify all listeners to event.
		if (OnMoveCountDecrease != null) {
			OnMoveCountDecrease();
		}
	}
}
