﻿using UnityEngine;

/// <summary>
/// Handles events to do with debug system.
/// </summary>
public class EventManagerDebug : MonoBehaviour {

	public delegate void EventHandlerDebugPanel();
	/// <summary>
	/// Event to listen to for when the Debug Panel opens.
	/// </summary>
	public static event EventHandlerDebugPanel OnDebugPanelOpen;

	/// <summary>
	/// Event to listen to for when the Debug Panel opens.
	/// </summary>
	public static event EventHandlerDebugPanel OnDebugPanelClose;

	/// <summary>
	/// Event to listen to for when the Debug Panel start a fake win condition.
	/// </summary>
	public static event EventHandlerDebugPanel OnFakeWin;


	/// <summary>
	/// Should be called when the Debug Panel opens.
	/// </summary>
	public static void OpenDebugPanel() {
		// notify all listeners to event.
		if (OnDebugPanelOpen != null) {
			OnDebugPanelOpen();
		}
	}

	/// <summary>
	/// Should be called when the Debug Panel closes.
	/// </summary>
	public static void CloseDebugPanel() {
		// notify all listeners to event.
		if (OnDebugPanelClose != null) {
			OnDebugPanelClose();
		}
	}

	/// <summary>
	/// Should be called when the want to create a fake win condition.
	/// </summary>
	public static void FakeWin() {
		// notify all listeners to event.
		if (OnFakeWin != null) {
			OnFakeWin();
		}
	}
}
