﻿using UnityEngine;

/// <summary>
/// Handles events to do with Collectables.
/// </summary>
public class EventManagerCollectable : MonoBehaviour {

	public delegate void EventHandlerCollectItem();
	/// <summary>
	/// Event to listen to for when Item has been collected.
	/// </summary>
	public static event EventHandlerCollectItem OnItemCollected;

	public delegate void EventHandlerCollectablePopUp(Vector3 worldPosition);
	/// <summary>
	/// Event to listen to for when a collectable PopUp should be played when collected.
	/// </summary>
	public static event EventHandlerCollectablePopUp OnSpawnPopUp;

	/// <summary>
	/// Should be called when a collectable has been collected.
	/// </summary>
	public static void ItemCollected() {
		// notify all listeners to event.
		if (OnItemCollected != null) {
			OnItemCollected();
		}
	}

	/// <summary>
	/// Should be called when a collectable has been collected.
	/// Will tell listeners to spawn any effects to show it has been collected.
	/// </summary>
	/// <param name="worldPosition"> The World Position of the collectable. </param>
	public static void SpawnCollectablePopUp(Vector3 worldPosition) {
		// notify all listeners to event.
		if (OnSpawnPopUp != null) {
			OnSpawnPopUp(worldPosition);
		}
	}
}

