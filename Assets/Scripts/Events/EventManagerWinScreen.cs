﻿using UnityEngine;

/// <summary>
/// Handles events to do with the Win Screen.
/// </summary>
public class EventManagerWinScreen : MonoBehaviour {

	public delegate void EventHandlerWinProgress();
	/// <summary>
	/// Event to listen to for when the camera has finished moving into position for the winscreen.
	/// </summary>
	public static event EventHandlerWinProgress OnCameraInPos;
	/// <summary>
	/// Event to listen to for move counts down.
	/// </summary>
	public static event EventHandlerWinProgress OnMoveCountDown;
	/// <summary>
	/// Event to listen to for when the MoveCounter has finished counting down.
	/// </summary>
	public static event EventHandlerWinProgress OnMoveCountDownFinished;

	public delegate void EventHandlerMoveCount(int countTotal);
	/// <summary>
	/// Event to listen to for total Move Count.
	/// </summary>
	public static event EventHandlerMoveCount OnMoveCountDownTotalMoves;


	/// <summary>
	/// Should be called when the camera is in the final win position.
	/// </summary>    
	public static void CameraInWinPosition() {
		// notify all listeners to event.
		if (OnCameraInPos != null) {
			OnCameraInPos();
		}
	}

	/// <summary>
	/// Should be called when move counter decreases a move.
	/// </summary>    
	public static void CountDownMove() {
		// notify all listeners to event.
		if (OnMoveCountDown != null) {
			OnMoveCountDown();
		}
	}

	/// <summary>
	/// Should be called when move counter has finished counting down moves.
	/// </summary>    
	public static void MoveCountDownFinished() {
		// notify all listeners to event.
		if (OnMoveCountDownFinished != null) {
			OnMoveCountDownFinished();
		}
	}

	/// <summary>
	/// Should be called when move counter Starts counting down.
	/// </summary>    
	public static void TotalMoveCount(int countTotal) {
		// notify all listeners to event.
		if (OnMoveCountDownTotalMoves != null) {
			OnMoveCountDownTotalMoves(countTotal);
		}
	}
}
