﻿using UnityEngine;

/// <summary>
/// Handles events to do with segments and focus changes.
/// </summary>
public class EventManagerSegment : MonoBehaviour {

	public delegate void EventHandlerSegment(Segment segment);

	/// <summary>
	/// Event to listen to for focus changing to Vertical Movement.
	/// </summary>
	public static event EventHandlerSegment OnSegmentMoveVertically;

	public delegate void EventHandlerSegmentUpdate();

	/// <summary>
	/// Event to listen for starting Vertical Movement.
	/// </summary>
	public static event EventHandlerSegmentUpdate OnSegmentMoveStarted;

	/// <summary>
	/// Event to listen for lose of focus in  Vertical Movement.
	/// </summary>
	public static event EventHandlerSegmentUpdate OnSegmentMoveReleased;

	/// <summary>
	/// Event to listen for segment being selected.
	/// </summary>
	public static event EventHandlerSegmentUpdate OnSegmentSelected;


	/// <summary>
	/// Should be called when the Segment Moves verticaly
	/// </summary>
	/// <param name="segment"> The segment that is being moved. </param>
	public static void SegmentMovedVertically(Segment segment) {
		// notify all listeners to event.
		if (OnSegmentMoveVertically != null) {
			OnSegmentMoveVertically(segment);
		}
	}

	/// <summary>
	/// Should be called when the Segment is released from movement controls.
	/// </summary>    
	public static void SegmentReleased() {
		// notify all listeners to event.
		if (OnSegmentMoveReleased != null) {
			OnSegmentMoveReleased();
		}
	}

	/// <summary>
	/// Should be called when the Segment starts to move veritcally.
	/// </summary>    
	public static void SegmentMoveStarted() {
		// notify all listeners to event.
		if (OnSegmentMoveStarted != null) {
			OnSegmentMoveStarted();
		}
	}

	/// <summary>
	/// Should be called when a Segment has been selected.
	/// </summary>    
	public static void SegmentSelected() {
		// notify all listeners to event.
		if (OnSegmentSelected != null) {
			OnSegmentSelected();
		}
	}
}
