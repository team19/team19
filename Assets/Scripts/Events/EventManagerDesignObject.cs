﻿using UnityEngine;

/// <summary>
/// Handles events to do with Design object.
/// Possible completions, and when the object has been completed
/// </summary>
public class EventManagerDesignObject : MonoBehaviour {

	public delegate void EventHandlerDesignObjectUpdate();
	/// <summary>
	/// Event to listen to for possible changes that may cause the object to be completed.
	/// </summary>
	public static event EventHandlerDesignObjectUpdate OnPossibleCompletion;
	/// <summary>
	/// Event to listen to for when the Object is completed.
	/// </summary>
	public static event EventHandlerDesignObjectUpdate OnCompletion;

	/// <summary>
	/// Should be called when there is a change in the game that could cause it to be completed.
	/// </summary>    
	public static void PossibleCompletion() {
		// notify all listeners to event.
		if (OnPossibleCompletion != null) {
			OnPossibleCompletion();
		}
	}

	/// <summary>
	/// Should be called when the Design Object is Completed.
	/// </summary>    
	public static void DesignObjectCompleted() {
		// notify all listeners to event.
		if (OnCompletion != null) {
			OnCompletion();
		}
	}
}
