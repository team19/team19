﻿using UnityEngine;

/// <summary>
/// Handles events to do with gallery Screen.
/// </summary>
public class EventManagerGallery : MonoBehaviour {

	public delegate void EventHandler();
	/// <summary>
	/// Event to listen to for when a new gallery Item is selected.
	/// </summary>
	public static event EventHandler OnNewGalleryItemSelected;
	/// <summary>
	/// Event to listen to for when the an gallery item is revealed.
	/// </summary>
	public static event EventHandler OnLightBulbInfoRevealed;

	public delegate void EventHandlerNewLevelInfo(LevelInfo newInfo);

	/// <summary>
	/// Event to listen to for when the ItemFrame in the gallery needs to be repopulated with new level information.
	/// </summary>
	public static event EventHandlerNewLevelInfo OnPopulateItemFrame;

	/// <summary>
	/// Should be called when a new gallery item is selected.
	/// </summary>
	public static void NewGalleryItemSelected() {
		// notify all listeners to event.
		if (OnNewGalleryItemSelected != null) {
			OnNewGalleryItemSelected();
		}
	}

	/// <summary>
	/// Should be called when a gallery light bulb info is revealed.
	/// </summary>
	public static void LightBulbInfoRevealed() {
		// notify all listeners to event.
		if (OnLightBulbInfoRevealed != null) {
			OnLightBulbInfoRevealed();
		}
	}

	/// <summary>
	/// Should be called when you want to Repopulate the Item Frame in the gallery with level info.
	/// </summary>
	public static void PopulateItemFrame(LevelInfo newInfo) {
		// notify all listeners to event.
		if (OnPopulateItemFrame != null) {
			OnPopulateItemFrame(newInfo);
		}
	}
}
