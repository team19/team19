﻿using UnityEngine;

/// <summary>
/// Handles events to do with Level selection and the difficulty select.
/// </summary>
public class EventManagerLevelSelect : MonoBehaviour {

	public delegate void EventHandler();
	/// <summary>
	/// Event to listen to for when the Difficulty Select should open.
	/// </summary>
	public static event EventHandler OnDifficultySelectOpened;

	/// <summary>
	/// Event to listen to for when a new Level Item is selected.
	/// </summary>
	public static event EventHandler OnNewLevelItemSelected;


	public delegate void EventHandlerNewLevelInfo(LevelInfo newInfo);
	/// <summary>
	/// Event to listen to for when the Difficulty Select needs to be repopulated with new level information.
	/// </summary>
	public static event EventHandlerNewLevelInfo OnPopulateDifficultySelect;

	/// <summary>
	/// Should be called when the Difficulty Select needs to open.
	/// </summary>
	public static void OpenDifficultySelect() {
		// notify all listeners to event.
		if (OnDifficultySelectOpened != null) {
			OnDifficultySelectOpened();
		}
	}

	/// <summary>
	/// Should be called when a new Level Item is selected.
	/// </summary>
	public static void NewLevelItemSelected() {
		// notify all listeners to event.
		if (OnNewLevelItemSelected != null) {
			OnNewLevelItemSelected();
		}
	}

	/// <summary>
	/// Should be called to Repopulate the Difficulty Select with level info.
	/// </summary>
	public static void PopulateDifficultySelect(LevelInfo newInfo) {
		// notify all listeners to event.
		if (OnPopulateDifficultySelect != null) {
			OnPopulateDifficultySelect(newInfo);
		}
	}
}
