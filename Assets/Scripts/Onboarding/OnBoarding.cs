﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controls the onboarding panels and panel buttons.
/// </summary>
public class OnBoarding : MonoBehaviour {
	[SerializeField]
	Camera screenCamera = null;

	[SerializeField]
	Button screenOneContinue = null;
	[SerializeField]
	GameObject screenOne =null;

	[SerializeField]
	Button screenTwoContinue = null;
	[SerializeField]
	GameObject screenTwo =null;

	[SerializeField]
	Button screenThreeContinue = null;
	[SerializeField]
	GameObject screenThree =null;

	[SerializeField]
	RotateTutorial rotateTutorial =null;

	/// <summary>
	/// Adds required listeners to buttons, and sets required panels active.
	/// </summary>
	private void Start() {
		screenOneContinue.onClick.AddListener(ContinueToScreenTwo);
		screenTwoContinue.onClick.AddListener(ContinueToScreenThree);
		screenThreeContinue.onClick.AddListener(StartRotateTutorial);

		screenOne.SetActive(true);
		screenTwo.SetActive(false);
		screenThree.SetActive(false);
	}

	/// <summary>
	/// Removes all listeners.
	/// </summary>
	private void OnDestroy() {
		screenOneContinue.onClick.RemoveListener(ContinueToScreenTwo);
		screenTwoContinue.onClick.RemoveListener(ContinueToScreenThree);
		screenThreeContinue.onClick.RemoveListener(StartRotateTutorial);
	}

	/// <summary>
	/// Enables Screen Two and disables all others.	
	/// </summary>
	private void ContinueToScreenTwo() {
		SoundBank.PlayButtonClick();
		screenOne.SetActive(false);
		screenTwo.SetActive(true);
	}

	/// <summary>
	/// Enables Screen Three and disables all others.
	/// </summary>
	private void ContinueToScreenThree() {
		SoundBank.PlayButtonClick();
		screenTwo.SetActive(false);
		screenThree.SetActive(true);
	}

	/// <summary>
	/// Start the next tutorial disabling itself.
	/// </summary>
	private void StartRotateTutorial() {
		SoundBank.PlayButtonClick();
		screenThree.SetActive(false);
		rotateTutorial.StartTutorial();
		screenCamera.enabled = false;
	}

}
