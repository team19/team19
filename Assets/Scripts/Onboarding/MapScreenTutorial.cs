﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controls the appearance of the Map tutorial overlay screens.
/// </summary>
public class MapScreenTutorial : MonoBehaviour {

	[SerializeField]
	GameObject selectCapsuleScreen =null;
	[SerializeField]
	Button levelDifficultyContinue = null;
	[SerializeField]
	GameObject selectLevelDifficultyScreen =null;

	[SerializeField]
	GameObject openTimeCapsuleScreen =null;

	[SerializeField]
	ScrollRect sideScrollMenu = null;

	/// <summary>
	/// Adds required listeners to buttons, and sets required panels active.
	/// </summary>
	private void Start() {
		SaveData mostRecentSave = SaveProgressSystem.RecentSave();
		if (mostRecentSave.finishedOnboarding) {
			gameObject.SetActive(false);
		}
		else {
			EventManagerLevelSelect.OnDifficultySelectOpened += StartDifficultyTutorial;
			sideScrollMenu.vertical = false;

			levelDifficultyContinue.onClick.AddListener(StartTimeCapsuleTutorial);
			selectCapsuleScreen.SetActive(true);
			selectLevelDifficultyScreen.SetActive(false);
			openTimeCapsuleScreen.SetActive(false);
		}
	}

	/// <summary>
	/// Removes all listeners.
	/// </summary>
	private void OnDestroy() {
		EventManagerLevelSelect.OnDifficultySelectOpened -= StartDifficultyTutorial;
		levelDifficultyContinue.onClick.RemoveListener(StartTimeCapsuleTutorial);
	}

	/// <summary>
	/// Move to the select difficulty screen when the Difficulty Select opens.
	/// </summary>
	private void StartDifficultyTutorial() {
		selectCapsuleScreen.SetActive(false);
		selectLevelDifficultyScreen.SetActive(true);
	}

	/// <summary>
	/// Move to the time Capsule screen when the level difficulty is finished.
	/// </summary>
	private void StartTimeCapsuleTutorial() {
		SoundBank.PlayButtonClick();
		selectLevelDifficultyScreen.SetActive(false);
		openTimeCapsuleScreen.SetActive(true);
	}
}
