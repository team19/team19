﻿using UnityEngine;
using UnityEngine.UI;

using LittleLot;

/// <summary>
/// Controls the appearance of the Move tutorials overlay screens.
/// </summary>
public class MoveTutorial : MonoBehaviour {

	[SerializeField]
	GameObject pressAndHoldScreen =null;

	[SerializeField]
	GameObject moveSegmentScreen =null;

	[SerializeField]
	Button wellDoneScreenContinue = null;
	[SerializeField]
	GameObject wellDoneScreen =null;

	[SerializeField]
	InputController inputController = null;

	[SerializeField]
	FingerAnimation finger = null;

	/// <summary>
	/// Ensures the objects is disabled at start on the correct screens are enabled.
	/// </summary>
	private void Start() {
		gameObject.SetActive(false);
		pressAndHoldScreen.SetActive(true);
		moveSegmentScreen.SetActive(false);
		wellDoneScreen.SetActive(false);
	}

	/// <summary>
	/// Ensures the Listeners are added when re-enabled.
	/// </summary>
	private void OnEnable() {
		wellDoneScreenContinue.onClick.AddListener(LoadMapLevel);

		EventManagerDesignObject.OnCompletion += ShowCompletionScreen;

		EventManagerSegment.OnSegmentMoveStarted += ShowMoveScreen;
		EventManagerSegment.OnSegmentMoveReleased += ShowPressAndHoldScreen;
	}

	/// <summary>
	/// Ensures all listeners are removed when disabled.
	/// </summary>
	private void OnDisable() {
		wellDoneScreenContinue.onClick.RemoveListener(LoadMapLevel);

		EventManagerDesignObject.OnCompletion -= ShowCompletionScreen;
		EventManagerSegment.OnSegmentMoveStarted -= ShowMoveScreen;
		EventManagerSegment.OnSegmentMoveReleased -= ShowPressAndHoldScreen;
	}

	/// <summary>
	/// Sets up the Input controller for this tutorial and the initial screens/animations.
	/// </summary>
	public void StartTutorial() {
		gameObject.SetActive(true);
		inputController.EnableControls();
		inputController.EnableLongPress();
		inputController.DisableFlickHorizontally();
		finger.PlayHoldAnimation();
	}

	/// <summary>
	/// Disables the entire tutorial object when it is finished.
	/// </summary>
	private void EndTutorial() {
		gameObject.SetActive(false);
	}

	/// <summary>
	/// Shows the press and hold to move animation and description, when not pressed.
	/// </summary>
	private void ShowPressAndHoldScreen() {
		pressAndHoldScreen.SetActive(true);
		moveSegmentScreen.SetActive(false);

	}

	/// <summary>
	/// Shows the move animation and screen when Pressed and held.
	/// </summary>
	private void ShowMoveScreen() {
		pressAndHoldScreen.SetActive(false);
		moveSegmentScreen.SetActive(true);
		finger.PlayDragAnimation();
	}

	/// <summary>
	/// Shows the completion screen when the design object is completed correctly.
	/// </summary>
	private void ShowCompletionScreen() {
		pressAndHoldScreen.SetActive(false);
		moveSegmentScreen.SetActive(false);
		wellDoneScreen.SetActive(true);
		finger.gameObject.SetActive(false);
	}

	/// <summary>
	/// Loads the map screen.
	/// </summary>
	private void LoadMapLevel() {
		SoundBank.PlayButtonClick();
		LoadingTransitionController.AnimatedLoadSceneAsync("MapScreen", "AnimatedLoadingScreen");
	}
}
