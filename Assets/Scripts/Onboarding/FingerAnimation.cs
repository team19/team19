﻿using UnityEngine;

/// <summary>
/// Controls the animations on the finger.
/// </summary>
[RequireComponent(typeof(Animator))]
public class FingerAnimation : MonoBehaviour {

	private Animator animator_ = null;

	/// <summary>
	/// Sets up the Animator.
	/// </summary>
	private void Start() {
		if (!animator_) {
			animator_ = GetComponent<Animator>();
		}
	}

	/// <summary>
	/// Plays the tap animation.
	/// </summary>
	public void PlayTapAnimation() {
		GetAnimator().Play("fingerTap");
	}

	/// <summary>
	/// Plays the flick animation
	/// </summary>
	public void PlayFlickAnimation() {
		GetAnimator().Play("fingerFlick");
	}

	/// <summary>
	/// Plays the press and hold animation
	/// </summary>
	public void PlayHoldAnimation() {
		GetAnimator().Play("fingerHold");
	}

	/// <summary>
	/// Plays the drag animation.
	/// </summary>
	public void PlayDragAnimation() {
		GetAnimator().Play("fingerDrag");
	}

	/// <summary>
	/// Ensures the animator is set as this class may be called before the object is initialised.
	/// </summary>
	private Animator GetAnimator() {
		if (!animator_) {
			animator_ = GetComponent<Animator>();
		}
		return animator_;
	}
}
