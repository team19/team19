﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controls the appearance of the Rotate tutorials overlay screens.
/// </summary>
public class RotateTutorial : MonoBehaviour {

	[SerializeField]
	GameObject tapToSelectScreen =null;

	[SerializeField]
	GameObject flickToRotateScreen =null;

	[SerializeField]
	Button wellDoneScreenContinue = null;
	[SerializeField]
	GameObject wellDoneScreen =null;


	[SerializeField]
	MoveTutorial moveTutorial=null;

	[SerializeField]
	InputController inputController = null;

	[SerializeField]
	FingerAnimation finger = null;

	/// <summary>
	/// Ensures the objects is disabled at start on the correct screens are enabled.
	/// </summary>
	private void Start() {
		gameObject.SetActive(false);

		tapToSelectScreen.SetActive(true);
		flickToRotateScreen.SetActive(false);
		wellDoneScreen.SetActive(false);
	}

	/// <summary>
	/// Ensures the Listeners are added when re-enabled.
	/// </summary>
	private void OnEnable() {
		wellDoneScreenContinue.onClick.AddListener(StartMoveTutorial);

		EventManagerSegment.OnSegmentSelected += ShowFlickToRotate;
		EventManagerDesignObject.OnCompletion += ShowCompletionScreen;

	}

	/// <summary>
	/// Ensures all listeners are removed when disabled.
	/// </summary>
	private void OnDisable() {
		wellDoneScreenContinue.onClick.RemoveListener(StartMoveTutorial);
		EventManagerSegment.OnSegmentSelected -= ShowFlickToRotate;
		EventManagerDesignObject.OnCompletion -= ShowCompletionScreen;
	}

	/// <summary>
	/// Sets up the Input controller for this tutorial and the initial screens/animations.
	/// </summary>
	public void StartTutorial() {
		gameObject.SetActive(true);
		inputController.EnableControls();
		inputController.DisableLongPress();
		inputController.EnableFlickHorizontally();

		finger.PlayTapAnimation();
	}

	/// <summary>
	/// Disables the entire tutorial object when it is finished.
	/// </summary>
	private void EndTutorial() {
		gameObject.SetActive(false);
	}

	/// <summary>
	/// Start the flick to rotate animation and description.
	/// Removes the listener so it only happens on the first call.
	/// </summary>
	private void ShowFlickToRotate() {
		finger.PlayFlickAnimation();
		EventManagerSegment.OnSegmentSelected -= ShowFlickToRotate;
		tapToSelectScreen.SetActive(false);
		flickToRotateScreen.SetActive(true);
	}

	/// <summary>
	/// Shows the completion screen when the design object is completed correctly.
	/// </summary>
	private void ShowCompletionScreen() {
		flickToRotateScreen.SetActive(false);
		wellDoneScreen.SetActive(true);
		finger.gameObject.SetActive(false);
	}

	/// <summary>
	/// Start the next tutorial disabling itself.
	/// Ensures the correct camera and UI is used for the next tutorial.
	/// </summary>
	private void StartMoveTutorial() {
		wellDoneScreen.SetActive(false);
		EndTutorial();
		moveTutorial.StartTutorial();
	}
}
