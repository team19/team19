﻿using System.Collections;

using UnityEngine;

using LittleLot;
/// <summary>
/// This class decides whether or not it should load the onboarding.
/// </summary>
public class OnBoardingDecider : MonoBehaviour {

	/// <summary>
	/// On Initial load determines based on save data which level to load next.
	/// Whether it should load the explore screen, or load the onboarding sequence.	
	/// </summary>
	private IEnumerator Start() {
		// Ensures it happens after everything else has been succesfully loaded.
		// Solves problem with some objects not being initialised fully (i.e. the transition controller)
		yield return new WaitForEndOfFrame();
		SaveData mostRecentSave = SaveProgressSystem.RecentSave();
		if (mostRecentSave.finishedOnboarding) {
			LoadingTransitionController.AnimatedLoadSceneAsync("ExploreScreen", "AnimatedLoadingScreen");
		}
		else {
			LoadingTransitionController.AnimatedLoadSceneAsync("OnBoardingSequence", "AnimatedLoadingScreen");
		}
	}

}
