﻿using UnityEngine;

/// <summary>
/// Handles interaction with segments by touch. Ray tracing for a valid segment.
/// and deciding if enough movement has happened to rotate it.
/// </summary>
public class InputController : MonoBehaviour {

	[SerializeField]
	[Tooltip("Amount of rotation each flick it turns.")]
	float rotationAngle = 45.0f;

	[SerializeField]
	[Tooltip("Multiplier for vertical segment movement speed.")]
	float moveSpeed = 20.0f;

	[Tooltip("Required x-axis movement before counted as a rotation.")]
	[SerializeField]
	float rotatationAllowedMovement = 30.0f;

	[Tooltip("Amount of time to wait before detected as a long press. ")]
	[SerializeField]
	float longPressDelay = 0.4f;

	[Tooltip("Alowed error in movement. Before long press not allowed.")]
	[SerializeField]
	float longPressAllowedMovement = 20.0f;

	private float longPressTimer_ = 0;

	private Segment currentSegment_;

	private Vector3 previousTouchPosition_;

	private Vector3 distanceFingerTravelled_;

	private bool canLongPress_ = true;

	private bool hasLongPressStarted_ = false;

	private bool canFlickHorizontally_ = true;

	private bool isInputEnabled_ = true;

	private bool wasTouching_ = false;

	private int initialSegmentOrder_ = 0;


	private bool hasFlickHorizontallyEnabled_ = true;

	private bool hasLongPressEnabled_ = true;

	/// <summary>
	/// Add listener to OnCompletion event, and pause open/close.
	/// To disable the controls, stopping the user from making changes when they have won, or paused the game.
	/// </summary>
	protected void OnEnable() {
		EventManagerDesignObject.OnCompletion += DisableControls;

		EventManagerPause.OnPauseMenuOpen += DisableControls;
		EventManagerPause.OnPauseMenuClose += EnableControls;

		EventManagerDebug.OnDebugPanelOpen += DisableControls;
		EventManagerDebug.OnDebugPanelClose += EnableControls;
	}

	/// <summary>
	/// Remove event listeners
	/// </summary>
	protected void OnDisable() {
		EventManagerDesignObject.OnCompletion -= DisableControls;

		EventManagerPause.OnPauseMenuOpen -= DisableControls;
		EventManagerPause.OnPauseMenuClose -= EnableControls;

		EventManagerDebug.OnDebugPanelOpen -= DisableControls;
		EventManagerDebug.OnDebugPanelClose -= EnableControls;
	}

	/// <summary>
	/// Make Sure all Event Listeners have been removed when destroying.
	/// </summary>
	protected void OnDestroy() {
		OnDisable();
	}

	/// <summary>
	/// Tracks Current User Interaction, and decides actions.
	/// </summary>
	protected void Update() {
		if (isInputEnabled_) {
			if (IsFirstTouch()) {
				UpdateSegmentHit();
				TrackInitialSegmentOrder();
				canFlickHorizontally_ = true;
			}

			if (IsTouching()) {
				wasTouching_ = true;
				// Update the distance the finger has moved since last press.
				distanceFingerTravelled_ = Input.mousePosition - previousTouchPosition_;

				if (IsLongPress()) {
					MoveVertically();
				}
				else if (IsFlickedHorizontally()) {
					RotateSegment();
				}
			}

			if (OnFinishTouching()) {
				longPressTimer_ = 0;
				canLongPress_ = true;
				hasLongPressStarted_ = false;
				if (currentSegment_) {
					currentSegment_.RemoveMovementFocus();

					EventManagerSegment.SegmentReleased();
					if (HasChangedOrder()) {
						SoundBank.PlayMoveSegment();
						EventManagerMoveCount.IncreaseMoveCount();
					}
				}
			}
		}
	}
	
	/// <summary>
	/// Disable the input controller, and removes focus from the currently selected segment.
	/// </summary>
	public void DisableControls() {
		isInputEnabled_ = false;
		if (currentSegment_) {
			currentSegment_.RemoveFocus();
			currentSegment_ = null;
		}
	}

	/// <summary>
	/// Reenables the input controller. 
	/// </summary>
	public void EnableControls() {
		isInputEnabled_ = true;
	}

	/// <summary>
	/// Enables the use of Long Presses for interaction.
	/// </summary>
	public void EnableLongPress() {
		hasLongPressEnabled_ = true;
	}

	/// <summary>
	/// Enables the use of flicks for interaction.
	/// </summary>
	public void EnableFlickHorizontally() {
		hasFlickHorizontallyEnabled_ = true;
	}

	/// <summary>
	/// Disables the use of Long Presses for interaction.
	/// </summary>
	public void DisableLongPress() {
		hasLongPressEnabled_ = false;
	}

	/// <summary>
	/// Disables the use of flicks for interaction.
	/// </summary>
	public void DisableFlickHorizontally() {
		hasFlickHorizontallyEnabled_ = false;
	}

	/// <summary>
	/// Check to see if this is the first time a finger has touched the screen.
	/// </summary>
	/// <returns>True the first time the screen is touched. </returns>
	protected bool IsFirstTouch() {
		return Input.GetMouseButtonDown(0);
	}

	/// <summary>
	/// Runs a Raycast from the touch point on the screen outwards in 3D Space to see if its hit a segment.
	/// Checks its a valid segment, either updates it to this new segment or changes to it.
	/// If not segment has been hit it disables long presses.
	/// </summary>
	protected void UpdateSegmentHit() {
		// ray trace to check if touching a segment.
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// if its hit anything
		if (Physics.Raycast(ray, out hit)) {
			// check its a segment
			if (hit.transform.gameObject.tag == "Segment") {
				EventManagerSegment.SegmentSelected();
				// If already has a segment selected
				if (currentSegment_) {
					// If it isn't the same segment as they are trying to hit
					if (currentSegment_ != hit.transform.gameObject.GetComponent<Segment>()) {
						// Remove the focus from the last segment
						currentSegment_.RemoveFocus();

						// Update to the new segment, and give it focus.
						currentSegment_ = hit.transform.gameObject.GetComponent<Segment>();
						currentSegment_.SetFocused();
					}
				}
				else {
					// If no segment has been hit yet just set the hit to focused.
					currentSegment_ = hit.transform.gameObject.GetComponent<Segment>();
					currentSegment_.SetFocused();
				}
			}
		}
		else {
			// if no segment has been hit, they cant use vertical movement. But can still rotate.
			canLongPress_ = false;
		}
		// Update the prevous touch position, so know where finger was when it was first pressed.
		previousTouchPosition_ = Input.mousePosition;

	}

	/// <summary>
	/// Update the segments initial position vertically within the design object.
	/// This is used to check if the object has been succesfully moved up or down when the player releases.
	/// </summary>
	protected void TrackInitialSegmentOrder() {
		if (currentSegment_) {
			initialSegmentOrder_ = currentSegment_.CurrentOrderNumber();
		}
	}

	/// <summary>
	/// If there is a finger/mouse touching the screen or not.
	/// </summary>
	/// <returns> True if a valid Segment selected and the screen is pressed. </returns>
	protected bool IsTouching() {
		return currentSegment_ && Input.GetMouseButton(0);
	}

	/// <summary>
	/// Checks if it can do a long press, and if it is a long press.
	/// Checks it can still do a long press, and that enough time has passed without any change in finger position.
	/// </summary>
	/// <returns> True if a long press has been achieved. </returns>
	protected bool IsLongPress() {
		if (canLongPress_ && hasLongPressEnabled_) {
			longPressTimer_ += Time.deltaTime;
			// Check if long press has been acheived.
			if (longPressTimer_ > longPressDelay) {

				currentSegment_.SetMovementFocus();
				return true;
			}
			// Check finger hasn't moved to much.
			if (Mathf.Abs(distanceFingerTravelled_.x) > longPressAllowedMovement) {
				canLongPress_ = false;
			}
		}
		return false;
	}

	/// <summary>
	/// Calls the design object to do a vertical movement of all other segments.
	/// Moves the Selected segment to match finger input. ( Change in height slide)
	/// Fires off an event to inform the Design Object that it needs to move everything to match this moving segment.
	/// </summary>
	protected void MoveVertically() {
		if (!hasLongPressStarted_) {
			// first move vertical
			hasLongPressStarted_ = true;
			EventManagerSegment.SegmentMoveStarted();
			SoundBank.PlayMoveSegment();
		}
		// calculaute the change in height since the last update.
		float yChange = distanceFingerTravelled_.y / Screen.height;

		// Move the object based on the change in height and the movespeed. Maintaining x/z position.
		currentSegment_.transform.position = new Vector3(currentSegment_.transform.position.x,
			currentSegment_.transform.position.y + (moveSpeed * yChange),
			currentSegment_.transform.position.z);
		// Update the prevous touch position, so know where finger was even with small movements.
		previousTouchPosition_ = Input.mousePosition;

		EventManagerSegment.SegmentMovedVertically(currentSegment_);
	}

	/// <summary>
	/// Can they flick and have they moved enough to cause a flick.
	/// </summary>
	/// <returns></returns>
	protected bool IsFlickedHorizontally() {
		if (canFlickHorizontally_ && hasFlickHorizontallyEnabled_) {
			// Check enough change in the x axis.
			if (Mathf.Abs(distanceFingerTravelled_.x) > rotatationAllowedMovement) {
				canFlickHorizontally_ = false;
				return true;
			}
		}
		return false;
	}

	/// <summary>
	/// Finished touching the screen and were previously touching a segment.
	/// Also enures this only happens once after having touched.
	/// </summary>
	/// <returns></returns>
	protected bool OnFinishTouching() {
		if (!IsTouching() && wasTouching_) {
			wasTouching_ = false;
			return true;
		}
		return false;
	}

	/// <summary>
	/// Checks to see if the finger has moved enough and rotates the Segment.
	/// </summary>
	protected void RotateSegment() {
		currentSegment_.RotateSegmemt(rotationAngle * -Mathf.Sign(distanceFingerTravelled_.x));
		previousTouchPosition_ = Input.mousePosition;
		// Ensure called last as ensures design object is in correct state.
		EventManagerMoveCount.IncreaseMoveCount();
		SoundBank.PlayRotateSegment();
	}

	/// <summary>
	/// Checks to see if the segment has changed in vertical order from when first moved.
	/// </summary>
	/// <returns> True if it has changed. </returns>
	protected bool HasChangedOrder() {
		if (currentSegment_.CurrentOrderNumber() == initialSegmentOrder_) {
			return false;
		}
		return true;
	}


}
