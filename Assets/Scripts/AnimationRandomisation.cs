﻿using System.Collections;

using UnityEngine;

/// <summary>
/// Simply utility class to alow randomization of provided animations.
/// </summary>
[RequireComponent(typeof(Animator))]
public class AnimationRandomisation : MonoBehaviour {
	[SerializeField]
	string[] animationNames = null;

	[SerializeField]
	float delayMin =1.0f;
	[SerializeField]
	float delayMax =10.0f;

	Animator animator_ = null;

	/// <summary>
	/// Starts the coroutine that randomly plays an animation from the list.	
	/// </summary>
	private void Start() {
		animator_ = GetComponent<Animator>();
		StartCoroutine(RandomiseAnimations());
	}

	/// <summary>
	/// Custom Coroutine picks a random delay within the class's range,
	/// and picks a random animation to play. Then waits a random delay period before doing 
	/// the same again, for the life of the object.
	/// </summary>
	/// <returns> Coroutine that runs till object destroyed. </returns>
	private IEnumerator RandomiseAnimations() {

		float delay = 0;
		string animation = animationNames[Random.Range(0, animationNames.Length)];
		while (enabled) {
			delay = Random.Range(delayMin, delayMax);
			animation = animationNames[Random.Range(0, animationNames.Length)];

			animator_.Play(animation);

			yield return new WaitForSeconds(delay);
		}
		yield return null;
	}
}
