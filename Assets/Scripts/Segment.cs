﻿using UnityEngine;

/// <summary>
/// Segment is the user controlled slice of the design object.
/// Stores the correct rotation, etc. sets intial start conditions for the segment.
/// As well as enabling user interaction with the segment.
/// </summary>
[RequireComponent(typeof(Renderer))]
public class Segment : MonoBehaviour {

	[SerializeField]
	Renderer outlinerRenderer =null;
	[SerializeField]
	int numberOfSegments;

	[SerializeField]
	float startRotation = 0.0f;

	[SerializeField]
	int correctOrderNumber = 0;

	private int currentOrderNumber_ = 0;

	private bool hasFocus_ = false;
	private bool hasMovementFocus_ = false;

	private bool isRotating_ = false;
	private float timeStartedRotating_ = 0.0f;
	private float rotationLerpSpeed_ = 10.0f;

	private float startRotationAngle_ = 0;
	private float endRotationAngle_ = 0;

	/// <summary>
	/// Max amount of error allowed in comparision check of rotation.
	/// </summary>
	private const float rotationEpsilon_ = 0.5f;

	/// <summary>
	/// Used for modifing shader settings on per segment basis. Used to allow segment highlighting.
	/// </summary>
	private Renderer renderer_;

	/// <summary>
	/// On Start up add get the Renderer of this device.
	/// </summary>
	private void Start() {
		renderer_ = GetComponent<Renderer>();
		renderer_.enabled = true;

		outlinerRenderer.enabled = true;
	}

	/// <summary>
	/// Make Sure all Event Listeners have been removed when destroying.
	/// </summary>
	private void OnDestroy() {
		RemoveFocus();
		// lazy hack to remove warning. as they are used in editor script but need to be kept persistant.
		startRotation = numberOfSegments + startRotation;

	}

	/// <summary>
	/// Whilst it is rotating do an angular lerp to the new point.
	/// </summary>
	private void Update() {
		if (isRotating_) {
			RotateToTarget();
		}
	}

	/// <summary>
	/// Used by editor script, sets to specific rotation amount in y.
	/// Ensures always looks correct in editor.
	/// </summary>
	/// <param name="rotationAmount"> The rotation to rotate by.</param>
	public void SetSegmentRotation(float rotationAmount) {
		transform.Rotate(Vector3.up, rotationAmount);
	}

	/// <summary>
	/// Segments rotation around the Y-axis.
	/// 
	/// Rotation in Degress to nearest integer.
	/// </summary>
	/// <returns> Rotation around y-axis in degrees. </returns>
	public float Rotation() {
		if (isRotating_) {
			return Mathf.Round(endRotationAngle_);// transform.localRotation.eulerAngles.y);
		}
		else {
			return Mathf.Round(transform.localRotation.eulerAngles.y);
		}
	}

	/// <summary>
	/// The current position vertically of the design object.
	/// </summary>
	/// <returns></returns>
	public int CurrentOrderNumber() {
		return currentOrderNumber_;
	}

	/// <summary>
	/// Sets the maximum available segments, for use with segment ordering.
	/// Allows the editor to know valid order numbers.
	/// </summary>
	/// <param name="maxSegments"></param>
	public void SetNumberOfSegments(int maxSegments) {
		numberOfSegments = maxSegments;
	}

	/// <summary>
	/// Used to set the current position vertically in the design object.
	/// </summary>
	/// <param name="orderNumber"></param>
	public void SetCurrentOrderNumber(int orderNumber) {
		currentOrderNumber_ = orderNumber;
	}

	/// <summary>
	/// Give focus to the segment to show it is selected.
	/// Done by changing shader settings. Calls focus changed event to ensure the UI is updated.
	/// </summary>    
	public void SetFocused() {
		if (!hasFocus_) {
			hasFocus_ = true;
			renderer_.material.SetFloat("_isSelected", 1.0f);
			outlinerRenderer.material.SetFloat("_isSelected", 1.0f);

		}
	}

	/// <summary>
	/// Removes focus from the segment to show it isnt being interacted with.
	/// Resets any changed shader or highlight settings back to normal.
	/// </summary>
	public void RemoveFocus() {
		if (hasFocus_) {
			hasFocus_ = false;
			renderer_.material.SetFloat("_isSelected", -1.0f);
			outlinerRenderer.material.SetFloat("_isSelected", -1.0f);
			RemoveMovementFocus();
		}
	}

	/// <summary>
	/// Sets up shader highlight effects to show the segment can be moved verticall.
	/// Done by changing movement highlighting shader settings.
	/// </summary>    
	public void SetMovementFocus() {
		if (!hasMovementFocus_) {
			renderer_.material.SetFloat("_isMoving", 1.0f);
			outlinerRenderer.material.SetFloat("_isMoving", 1.0f);
			hasMovementFocus_ = true;
		}

	}

	/// <summary>
	/// Removes shader highlight effects.
	/// Resetting any changed movement highlighting settings back to normal.
	/// </summary>
	public void RemoveMovementFocus() {
		if (hasMovementFocus_) {
			renderer_.material.SetFloat("_isMoving", -1.0f);
			outlinerRenderer.material.SetFloat("_isMoving", -1.0f);
			hasMovementFocus_ = false;
		}
	}

	/// <summary>
	/// Checks if this segment is in the correct order.
	/// </summary>
	/// <returns> True if in correct order. </returns>
	public bool InCorrectOrder(int orderNumber) {
		if (correctOrderNumber == orderNumber) {
			return true;
		}
		return false;
	}

	/// <summary>
	/// Compares segments rotation against given rotation value.
	/// Returns true if rotations match.
	/// </summary>
	/// <param name="nextObjectRotation"></param>
	/// <returns> True if rotations match</returns>
	public bool CheckRotation(float nextObjectRotation) {

		// Fixes problem of both rotations really being at 0, but one says 0.
		// So when calculate difference later, shows as 360.
		float left = Rotation();
		// Catchs a bunch of edge cases, caused by lerping, and EndAngles ability to be off the 0-360 (as want to ensure direction of lerp)
		// Unity has a habit of being unsure how it wants to serialize the angle ( sometimes 0 to 360, sometimes -180 to 180 )
		if (left == 360) {
			left = 0;
		}
		if (left < 0) {
			left += 360;
		}
		if (left > 360) {
			left -= 360;
		}
		if (nextObjectRotation == 360) {
			nextObjectRotation = 0;
		}
		if (nextObjectRotation < 0) {
			nextObjectRotation += 360;
		}
		if (nextObjectRotation > 360) {
			nextObjectRotation -= 360;
		}
		if ((Mathf.Abs(left - nextObjectRotation)) < rotationEpsilon_) {
			return true;
		}
		return false;
	}

	/// <summary>
	/// Rotates the segment by the rotation amount with an angular lerp.
	/// Starts the lerping to the new rotation.
	/// </summary>
	/// <param name="rotationAmount">Amount of rotation in degrees. </param>
	public void RotateSegmemt(float rotationAmount) {
		// Set when it started.
		timeStartedRotating_ = Time.time;

		if (!isRotating_) {
			// Find the end angle.
			endRotationAngle_ = transform.localRotation.eulerAngles.y + rotationAmount;
		}
		else {
			// Otherwise add it to the current end angle. 
			// Adding straight to eulerangles will cause errors as could be half way through a rotation.
			endRotationAngle_ = endRotationAngle_ + rotationAmount;
		}
		startRotationAngle_ = transform.localRotation.eulerAngles.y;
		isRotating_ = true;
		EventManagerDesignObject.PossibleCompletion();
	}

	/// <summary>	
	/// Lerps the y-axis angle between the start and end angles by a percentage amplified by lerpSpeed.
	/// </summary>
	private void RotateToTarget() {

		float timeSinceStarted = Time.time - timeStartedRotating_;
		// Calculate the percentage amount we wont to move this frame, then multiple by the speed.
		float percentageComplete = timeSinceStarted * rotationLerpSpeed_;

		// Hold x,z fixed whilst y is Angulary lerped between start and end angles.	
		transform.localRotation = Quaternion.Euler(new Vector3(
			transform.localRotation.eulerAngles.x,
			Mathf.LerpAngle(startRotationAngle_, endRotationAngle_, percentageComplete),
			transform.localRotation.eulerAngles.z));

		// Check to see if its completed.
		if (percentageComplete >= 1.0f) {
			// If so stop rotating and set it to the exact point.
			// Just incase of floating rounding errors.
			transform.localRotation = Quaternion.Euler(new Vector3(
				transform.localRotation.eulerAngles.x,
				endRotationAngle_,
				transform.localRotation.eulerAngles.z));
			isRotating_ = false;
		}
	}

}
