﻿using UnityEngine;

/// <summary>
/// Scriptable Object to be used to describe all the different level data.
/// </summary>
[CreateAssetMenu(fileName = "NewLevelInfo", menuName = "BluePix/Level Info", order = 1)]
public class LevelInfo : ScriptableObject {

	public string hiddenName = "Unknown";
	public string displayName = "Still Unknown";

	public string designerName = "Designer Not Set";

	public Sprite hiddenIcon = null;
	public Sprite displayIcon = null;
	public Sprite galleryIcon = null;
	public Sprite galleryRenderLit = null;
	public Sprite galleryRenderUnlit = null;
	public string[] lightBulbInfo = { "LightBulb 1 Info", "LightBulb 2 Info", "LightBulb 3 Info" };

	public string[] sceneNames = { "MapScreen","MapScreen","MapScreen" };
	public int[] threeStarMaxMoves = { 6, 6, 6 };
	public int[] twoStarMaxMoves = { 16, 16, 16 };
}

