﻿using UnityEngine;

/// <summary>
/// Scriptable Object to be used to describe all the different Categories and levels.
/// </summary>
[CreateAssetMenu(fileName = "NewMapInfo", menuName = "BluePix/Map Info", order = 2)]
public class MapInfo : ScriptableObject {

	public Category[] categories =  null;
}

/// <summary>
///	Holder class for catergory information.
/// Allows MapInfo to be in-line except for levels in the editor.
/// </summary>
[System.Serializable]
public class Category {
	public string categoryName = "Unknown";
	public LevelInfo[] levelInfos =  null;
}

