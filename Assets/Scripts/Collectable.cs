﻿using UnityEngine;

using LittleLot;

/// <summary>
/// Collectable script adds simple animation effects and increments collected counter.
/// </summary>
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(IdleAnimation))]
public class Collectable : MonoBehaviour {

	private bool isCollectable_ =true;

	private Renderer renderer_;

	private IdleAnimation idleAnimator_ = null;

	/// <summary>
	/// Store initial start and end points, and ensures tag is correctly set.
	/// </summary>
	private void Start() {
		gameObject.tag = "Collectable";

		if (!renderer_) {
			renderer_ = GetComponent<Renderer>();
		}
		if (!idleAnimator_) {
			idleAnimator_ = GetComponent<IdleAnimation>();
		}
	}

	/// <summary>
	/// Add listener to Move Started and move finished event.
	/// To disable the controls, stopping the user from making changes when they have won, or paused the game.
	/// </summary>
	private void OnEnable() {
		EventManagerSegment.OnSegmentMoveStarted += DisableCollectable;
		EventManagerSegment.OnSegmentMoveReleased += EnableCollectable;
	}

	/// <summary>
	/// Remove event listeners
	/// </summary>
	private void OnDisable() {
		EventManagerSegment.OnSegmentMoveStarted -= DisableCollectable;
		EventManagerSegment.OnSegmentMoveReleased -= EnableCollectable;
	}

	/// <summary>
	/// Ensures tag is set properly in editor.
	/// </summary>
	private void OnValidate() {
		// SendMessage cannot be called during Awake, CheckConsistency, or OnValidate << Requires investigation
		//gameObject.tag = "Collectable";
	}

	/// <summary>
	/// When it collides with a slice disables itself and calls ItemCollected Event.
	/// </summary>
	/// <param name="other"> The collider it has collided with. </param>
	private void OnTriggerStay(Collider other) {
		if (other.tag == "Slice" && isCollectable_) {
			EventManagerCollectable.SpawnCollectablePopUp(gameObject.transform.position);
			EventManagerCollectable.ItemCollected();
			idleAnimator_.PauseAnimation();
			gameObject.SetActive(false);
		}
	}

	/// <summary>
	/// Disables interaction when segment moving vertically.
	/// Disables animation, and sets it to faded out.
	/// </summary>
	private void DisableCollectable() {
		isCollectable_ = false;
		idleAnimator_.PauseAnimation();
		renderer_.material.SetFloat("_isFaded", 1.0f);
	}

	/// <summary>
	/// Enables interaction when segment stops moving vertically.
	/// Enables animation to previous state, and sets it to normal colour.
	/// </summary>
	private void EnableCollectable() {
		isCollectable_ = true;
		idleAnimator_.ResumeAnimation();
		renderer_.material.SetFloat("_isFaded", -1.0f);
	}

}
