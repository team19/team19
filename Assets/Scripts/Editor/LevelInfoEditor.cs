﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Custom editor inspector script for Level Information.
/// Reduces the chance of user error when setting up level descriptions.
/// </summary>
[CustomEditor(typeof(LevelInfo))]
public class LevelInfoEditor : Editor {

	private SerializedObject levelInfoObject_;

	private SerializedProperty hiddenName_;

	private SerializedProperty displayName_;

	private SerializedProperty designerName_;

	private SerializedProperty hiddenIcon_;

	private SerializedProperty displayIcon_;

	private SerializedProperty galleryIcon_;
	private SerializedProperty galleryRenderLit_;
	private SerializedProperty galleryRenderUnlit_;
	private SerializedProperty lightBulbInfo_;

	private SerializedProperty sceneNames_;
	private SerializedProperty twoStarMaxMoves_;
	private SerializedProperty threeStarMaxMoves_;

	private static bool[] showFoldout = { true,true,true };
	/// <summary>
	/// When enabled it stores a refrence to all the serialized properties in the Editor inspector. 
	/// </summary>
	private void OnEnable() {
		levelInfoObject_ = new SerializedObject(target);

		hiddenName_ = levelInfoObject_.FindProperty("hiddenName");
		displayName_ = levelInfoObject_.FindProperty("displayName");
		designerName_ = levelInfoObject_.FindProperty("designerName");

		hiddenIcon_ = levelInfoObject_.FindProperty("hiddenIcon");
		displayIcon_ = levelInfoObject_.FindProperty("displayIcon");

		galleryIcon_ = levelInfoObject_.FindProperty("galleryIcon");
		galleryRenderLit_ = levelInfoObject_.FindProperty("galleryRenderLit");
		galleryRenderUnlit_ = levelInfoObject_.FindProperty("galleryRenderUnlit");
		lightBulbInfo_ = levelInfoObject_.FindProperty("lightBulbInfo");

		sceneNames_ = levelInfoObject_.FindProperty("sceneNames");
		twoStarMaxMoves_ = levelInfoObject_.FindProperty("twoStarMaxMoves");
		threeStarMaxMoves_ = levelInfoObject_.FindProperty("threeStarMaxMoves");
	}

	/// <summary>
	/// Custom Representation of the Editor Inspector, Tries to ensure the level description can't be broken.
	/// </summary>
	public override void OnInspectorGUI() {
		levelInfoObject_.Update();

		EditorGUILayout.PropertyField(hiddenName_,
			new GUIContent("Hidden name", "The name Displayed when level is locked."));

		EditorGUILayout.PropertyField(displayName_,
			new GUIContent("Display name", "The name Displayed when level is unlocked."));

		EditorGUILayout.PropertyField(designerName_,
			new GUIContent("Designer Name", "The Designers name Displayed under the object title."));


		EditorGUILayout.PropertyField(hiddenIcon_,
			new GUIContent("Hidden Icon", "The Icon Displayed when level is locked."));
		EditorGUILayout.PropertyField(displayIcon_,
			new GUIContent("Display Icon", "The Icon Displayed when level is unlocked."));

		EditorGUILayout.PropertyField(galleryIcon_,
		new GUIContent("Gallery Icon", "The Icon Displayed in the gallery selection panel."));

		EditorGUILayout.PropertyField(galleryRenderLit_,
			new GUIContent("Gallery Render Lit", "The High quality object render shown in the gallery whenthe object is discovered."));

		EditorGUILayout.PropertyField(galleryRenderUnlit_,
			new GUIContent("Gallery Render Unlit", "The High quality object render shown in the gallery when the object is Unknown."));

		// Expose the 3 scenes with there other variables in a tab.
		for (int i = 0; i < sceneNames_.arraySize; i++) {

			showFoldout[i] = EditorGUILayout.Foldout(showFoldout[i], "Level " + (i + 1).ToString());

			if (showFoldout[i]) {
				EditorGUI.indentLevel++;

				EditorGUILayout.PropertyField(sceneNames_.GetArrayElementAtIndex(i),
					new GUIContent("Scene name ", "The scene name of the level of difficulty."));
				EditorStyles.textField.wordWrap = true;
				EditorGUILayout.PropertyField(lightBulbInfo_.GetArrayElementAtIndex(i),
					new GUIContent("Light Bulb Info", "The information to display for this lightbulb."),
					GUILayout.ExpandHeight(true), GUILayout.MaxHeight(50));

				EditorGUILayout.PropertyField(twoStarMaxMoves_.GetArrayElementAtIndex(i),
					new GUIContent("Two Star Move", "The maximum number of moves allowed for 3 stars."));

				EditorGUILayout.PropertyField(threeStarMaxMoves_.GetArrayElementAtIndex(i),
					new GUIContent("Three Star Move", "The maximum number of moves allowed for 3 stars."));

				EditorGUI.indentLevel--;
			}
		}

		levelInfoObject_.ApplyModifiedProperties();
	}

}

