﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Custom editor inspector script for Unitys Editor Inspector.
/// Reduces the chance of user error when creating segments.
/// </summary>
[CustomEditor(typeof(Segment))]
public class SegmentEditor : Editor {

	private SerializedObject segmentObject_;

	private SerializedProperty outlinerRenderer_;

	private SerializedProperty numberOfSegments_;

	private SerializedProperty correctOrder_;

	private SerializedProperty startRotation_;

	private Segment segment_;

	/// <summary>
	/// When enabled it stores a refrence to all the serialized properties in the Editor inspector.
	/// </summary>
	private void OnEnable() {
		segmentObject_ = new SerializedObject(target);
		segment_ = (Segment)(target);

		outlinerRenderer_ = segmentObject_.FindProperty("outlinerRenderer");

		numberOfSegments_ = segmentObject_.FindProperty("numberOfSegments");

		correctOrder_ = segmentObject_.FindProperty("correctOrderNumber");

		startRotation_ = segmentObject_.FindProperty("startRotation");

		segmentObject_.Update();

		// Ensure it is tagged as a segment.
		if (segment_.tag != "Segment") {
			segment_.tag = "Segment";
		}

		segmentObject_.ApplyModifiedProperties();
	}
	
	/// <summary>
	/// Custom Representation of the Editor Inspector.
	/// Tries to ensure the object can't be broken in game.
	/// </summary>
	public override void OnInspectorGUI() {
		segmentObject_.Update();

		EditorGUILayout.PropertyField(outlinerRenderer_,
			new GUIContent("Outliner Renderer", "Renderer used to add internal outline render to segment piece."));


		GUILayout.Label("Correct State", EditorStyles.boldLabel);

		EditorGUILayout.IntSlider(correctOrder_, 0, numberOfSegments_.intValue,
				new GUIContent("Correct Order",
				"The order vertically it is required to be in for the level to finish. "));

		GUILayout.Label("Initial State", EditorStyles.boldLabel);

		segment_.transform.localRotation = Quaternion.identity;
		segment_.transform.localPosition = new Vector3(0, segment_.transform.localPosition.y, 0);
		EditorGUILayout.Slider(startRotation_, 0.0f, 360.0f,
			new GUIContent("Start Rotation", "Rotation to start each level at. "));
		segment_.SetSegmentRotation(startRotation_.floatValue);

		segmentObject_.ApplyModifiedProperties();
	}

}
