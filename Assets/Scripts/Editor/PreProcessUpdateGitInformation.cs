﻿using UnityEditor.Build;
using UnityEditor.Build.Reporting;

/// <summary>
/// Extends IPreprocessBuildWithReport to update the Git commit and branch before a build.
/// <para/>
/// <remarks>
/// This ensures the git information supplied to the build is up to date.
/// </remarks>
/// </summary>
public class PreProcessUpdateGitInformation : IPreprocessBuildWithReport {

	public int callbackOrder { get { return 1; } }

	/// <summary>
	/// Updates the Git commit and branch in GitInfo before building.
	/// </summary>
	/// <param name="report"> Current Build report. </param>
	public void OnPreprocessBuild(BuildReport report) {
		LittleLot.CommonGitCommands.UpdateGitInformation();
	}
}
