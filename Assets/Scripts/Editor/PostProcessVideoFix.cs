﻿using UnityEditor.Build;
using UnityEditor.Build.Reporting;

/// <summary>
/// Extends IPostprocessBuild to remove the VideoDecode Shader after a build.
/// <para/>
/// <remarks> 
/// This stops the VCS getting polluted with video decode shaders for different platforms 
/// after a build.
/// </remarks>
/// </summary>
public class PostProcessVideoFix : IPostprocessBuildWithReport {

	public int callbackOrder { get { return 0; } }

	/// <summary>
	/// Removes all the shaders for Unity Video after building.
	/// <para/>
	/// <remarks> 
	/// Ensures source control isn't cluttered with incorrect platform shaders.
	/// </remarks>
	/// </summary>
	/// <param name="report"> Current Build report. </param>
	public void OnPostprocessBuild(BuildReport report) {
		LittleLot.VideoShaderBuildFix.RemoveAllVideoDecodeShaders();
	}
}

