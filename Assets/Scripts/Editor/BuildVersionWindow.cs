﻿using System;

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

/// <summary>
/// The Version Number handling Window, allows manually changing of major/minor easily.
/// </summary>
public class BuildVersionWindow : EditorWindow {

	private static string currentVersion_;

	private static int major_;

	private static int minor_;

	private static int patch_;

	/// <summary>
	/// Checks whether the BundleVersion is valid, if not sets it to a default value.
	///
	/// Quick tool for making it easier to setup automatic bundle version with commit version.
	/// </summary>
	private static void ValidateBundleVersion() {
		try {
			currentVersion_ = PlayerSettings.bundleVersion;
			major_ = Convert.ToInt32(currentVersion_.Split('.')[0]);
			minor_ = Convert.ToInt32(currentVersion_.Split('.')[1]);
			patch_ = Convert.ToInt32(currentVersion_.Split('.')[2]);
		} catch (Exception) {
			PlayerSettings.bundleVersion = "0.0.0";
			UnityEngine.Debug.LogError("Invalid BundleVersion setting to 0.0.0 ");
			currentVersion_ = PlayerSettings.bundleVersion;
			major_ = Convert.ToInt32(currentVersion_.Split('.')[0]);
			minor_ = Convert.ToInt32(currentVersion_.Split('.')[1]);
			patch_ = Convert.ToInt32(currentVersion_.Split('.')[2]);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}
	}
	
	/// <summary>
	/// Expose the Build version Manager in the editor.
	/// Simplifies the proccess and ensures everything is on the same style.
	/// </summary>
	[MenuItem("Tools/Manage Version Number")]
	public static void ShowWindow() {
		ValidateBundleVersion();
		GetWindow(typeof(BuildVersionWindow),true);
	}

	/// <summary>
	/// Renders a custom editor window to expose certain settings.
	/// </summary>
	void OnGUI() {
		ValidateBundleVersion();
		titleContent.text = "Build Version";
		GUILayout.Label("Current Version: " + currentVersion_, EditorStyles.boldLabel);

		DisplayVersionIncrementor("Major: ", ref major_);
		DisplayVersionIncrementor("Minor: ", ref minor_);
		DisplayVersionIncrementor("Patch: ", ref patch_);
	}

	private void DisplayVersionIncrementor(string name, ref int value) {
		GUILayout.BeginHorizontal();
		GUILayout.Label(name + value.ToString());
		if (GUILayout.Button("▲", GUILayout.Width(20f))) {
			value++;
			PlayerSettings.bundleVersion = string.Format("{0}.{1}.{2}", major_, minor_, patch_);
		}
		if (GUILayout.Button("▼", GUILayout.Width(20f))) {
			value--;
			PlayerSettings.bundleVersion = string.Format("{0}.{1}.{2}", major_, minor_, patch_);
		}
		GUILayout.EndHorizontal();
	}
	
}
