﻿using UnityEditor;

/// <summary>
/// Exposes useful features for debugging serialization issues
/// </summary>
public static class SerializationExtensions
{
	/// <summary>
	/// Exposes AssetDatabase.ForceReserializeAssets to UI.
	/// </summary>
	/// <remarks> Really useful after a Version update as forces everything to the new format. </remarks>
	[MenuItem("Assets/Force Re-serialization")]
	private static void ForceReSerialization() {
		AssetDatabase.ForceReserializeAssets();
	}
}

