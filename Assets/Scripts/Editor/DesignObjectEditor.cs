﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Custom editor inspector script for Design Objects.
/// Reduces the chance of user error when setting up design objects.
/// </summary>
[CustomEditor(typeof(DesignObject))]
public class DesignObjectEditor : Editor {

	private SerializedObject serializedDesignObject_;

	private DesignObject designObject_;

	private SerializedProperty segmentsArray_;

	private SerializedProperty segmentHeight_;

	/// <summary>
	/// When enabled it stores a refrence to all the serialized properties in the Editor inspector. 
	/// </summary>
	private void OnEnable() {
		serializedDesignObject_ = new SerializedObject(target);
		designObject_ = (DesignObject)(target);

		segmentsArray_ = serializedDesignObject_.FindProperty("segments");
		segmentHeight_ = serializedDesignObject_.FindProperty("segmentHeight");

		serializedDesignObject_.Update();

		// SetNumberOfSegments for all segments.
		for (int i = 0; i < segmentsArray_.arraySize; i++) {
			GetSegmentAtIndex(i).SetNumberOfSegments(segmentsArray_.arraySize - 1);
		}

		designObject_.RecalculateAllSegmentPositions();

		serializedDesignObject_.ApplyModifiedProperties();
	}

	/// <summary>
	/// Custom Representation of the Editor Inspector.
	/// Tries to ensure the object can't be broken in game.
	/// </summary>
	public override void OnInspectorGUI() {
		serializedDesignObject_.Update();

		segmentHeight_.floatValue = EditorGUILayout.FloatField(
			new GUIContent("Segment Height", "The uniform vertical spacing between ech segment. "),
			segmentHeight_.floatValue);

		for (int i = 0; i < segmentsArray_.arraySize; i++) {

			GUILayout.BeginHorizontal();

			// Show the segment on the screen allowing the user to drag and drop items to specfic
			// array items, and show the current position of the segment with its name.
			var result = EditorGUILayout.ObjectField(
				GetSegmentAtIndex(i), typeof(Segment), true) as Segment;

			// Check if the user has drag/dropped an item into the array item.
			if (GUI.changed) {
				// Try to add it to the array, will not get here if its not a valid segment.
				SetSegmentAtIndex(i, result);
			}

			// Store the current state of the UI enabled.
			var oldEnabled = GUI.enabled;

			// If it can move up set it to enabled only if it is already enabled.
			GUI.enabled &= CanMoveUp(i);
			if (GUILayout.Button("▲", GUILayout.Width(20f))) {
				SwapSegments(i, i - 1); // Move Up by swapping.
			}

			// If it can move up set it to enabled only if it is already enabled.
			GUI.enabled = oldEnabled & CanMoveDown(i);
			if (GUILayout.Button("▼", GUILayout.Width(20f))) {
				SwapSegments(i, i + 1); // Move down by swapping.
			}

			// Reset the Ui to the old state.
			GUI.enabled = oldEnabled;
			if (GUILayout.Button("-", GUILayout.Width(40.0f))) {
				RemoveSegmentAtIndex(i);
			}
			GUI.enabled = oldEnabled;

			GUILayout.EndHorizontal();
		}

		DropAreaGui();

		serializedDesignObject_.ApplyModifiedProperties();

		// Recalculate positions, after all changes have been applied.
		serializedDesignObject_.Update();
		designObject_.RecalculateAllSegmentPositions();
		serializedDesignObject_.ApplyModifiedProperties();
	}

	/// <summary>
	/// Get the Segment refrence within the segments array at the given index.
	/// </summary>
	/// <param name="index"> The index of the segment you are after in the segments array. </param>
	/// <returns> Segment Object within the array. </returns>
	private Segment GetSegmentAtIndex(int index) {
		return segmentsArray_.GetArrayElementAtIndex(index).objectReferenceValue as Segment;
	}

	/// <summary>
	/// Overrides the segment refrence at the provided index, with the provided segment.
	/// </summary>
	/// <param name="index"> Index in segments array to change. </param>
	/// <param name="segment"> Refrence value to change it to. </param>
	private void SetSegmentAtIndex(int index, Segment segment) {
		segmentsArray_.GetArrayElementAtIndex(index).objectReferenceValue = segment;
	}

	/// <summary>
	/// Removes the refrence to a segment in the array at the given index.    
	/// Reduces the array by one, after moving all remaining segments up one position,
	///  to fill the gap left by the Remove.
	/// <para> Doesn't delete the Segment Object itself. </para>
	/// </summary>
	/// <param name="index"> Position in the array to remove. </param>
	private void RemoveSegmentAtIndex(int index) {
		for (int i = index; i < (segmentsArray_.arraySize - 1); i++) {
			SetSegmentAtIndex(i, GetSegmentAtIndex(i + 1));
		}
		segmentsArray_.arraySize--;

	}

	/// <summary>
	/// Move an array element from the source array position to the destination position,
	/// and move the destination element to the source position.
	/// <para> Source and destination should not match as pointless! </para>
	/// </summary>
	/// <param name="sourceIndex"> Index position to swap from. </param>
	/// <param name="destinationIndex"> Index position to swap with. </param>
	private void SwapSegments(int sourceIndex, int destinationIndex) {
		segmentsArray_.MoveArrayElement(destinationIndex, sourceIndex);
	}

	/// <summary>
	/// Segment can be swaped with an item in the array above it.
	/// Simple array bounds check.
	/// </summary>
	/// <param name="index"> Index refrence to the segment being moved. </param>
	/// <returns> True if it can be moved. </returns>
	private bool CanMoveUp(int index) {
		return index >= 1;
	}

	/// <summary>
	/// Segment can be swaped with an item in the array below it.
	/// Simple array bounds check.
	/// </summary>
	/// <param name="index"> Index refrence to the segment being moved. </param>
	/// <returns> True if it can be moved. </returns>
	private bool CanMoveDown(int index) {
		return index < segmentsArray_.arraySize - 1;
	}

	/// <summary>
	/// Creates a new array element at the end of the array.
	/// Sets it Segment refrence value to the provided segment.
	/// </summary>
	/// <param name="segment"> Segment to add to the array. </param>
	private void AddSegment(Segment segment) {
		segmentsArray_.InsertArrayElementAtIndex(segmentsArray_.arraySize);
		segmentsArray_.GetArrayElementAtIndex(segmentsArray_.arraySize - 1).objectReferenceValue = segment;

	}

	/// <summary>
	/// Allows the user to drag and drop 1 or multiple Segments to create and add segments to the Object.
	/// Will create the space in the array and add all the segments to it.
	/// </summary>
	private void DropAreaGui() {

		// Store the current event.
		Event evt = Event.current;
		// Create a Rectangle in the current screen space (using GetRect).
		Rect dropArea = GUILayoutUtility.GetRect(0, 50, GUILayout.ExpandWidth(true));
		// Create a box based on the Rect used to allow interaction, an area for the user to drop the segment.
		GUI.Box(dropArea, "Drag and Drop Segment here");

		// If a valid event type, i.e dragging a gameobject of some kind.
		switch (evt.type) {

			case EventType.DragUpdated:
			case EventType.DragPerform:
				// Check if it is within the drop area if not finish early.
				if (!dropArea.Contains(evt.mousePosition)) {
					break;
				}
				// Change the mouse icon to the Unity Copy icon,
				// so the user know they can drop/add stuff inside this box.
				DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

				// Check it has dropped it aswell.
				if (evt.type == EventType.DragPerform) {
					// Tell the rest of the UI they tryed to drop it on this element,
					// so no one else should try to use it.    
					DragAndDrop.AcceptDrag();
					// Iterate through all the objects being dragged.
					foreach (var draggedObject in DragAndDrop.objectReferences) {
						GameObject currentDraggedObject = draggedObject as GameObject;
						// If the Dragged object isnt a game object we can can try the next object.
						if (!currentDraggedObject) {
							continue;
						}
						// If the Dragged object isnt a Segment we can try the next object.
						Segment segment = currentDraggedObject.GetComponent<Segment>();
						if (!segment) {
							continue;
						}
						// Add it to the array of segments.
						AddSegment(segment);
					}
				}
				// Tell the rest of the UI this event has been used.
				Event.current.Use();
				break;
			default:
				break;
		}
	}

}
