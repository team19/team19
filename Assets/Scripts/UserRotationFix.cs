﻿using UnityEngine;

/// <summary>
/// Ensures the users device AutoRotation setting is honored.
/// 
/// This is useful for Unity as it does not track whether the user has disabled
/// auto rotate. This class allows you to set allowed rotations whilst
/// still honoring player rotation settings.i.e. not turning from portrait to 
/// landscape when AutoRotate is Disabled.
/// </summary>
public class UserRotationFix : MonoBehaviour {

	[SerializeField]
	[Tooltip("Whether this rotationFix should exist throughout all scenes.")]
	bool isPersistant = false;

	/// <summary>
	/// Possible Available Rotation Settings.
	/// </summary>
	private enum AutoOrientation {
		kAutoPortrait,
		kAutoLandscape,
		kAutoAll,
		kNoAuto
	}

	[SerializeField]
	private AutoOrientation allowedOrientation = AutoOrientation.kAutoPortrait;

	/// <summary>
	/// Sets it to not be destroyed on load if it is meant to be persistant.
	/// Also checks if a persistant rotation already exists and deletes it.
	/// </summary>
	void Start() {
		if (isPersistant) {
			GameObject oldRotationFix = GameObject.Find("PersistentRotationFix");
			if (oldRotationFix) {
				DestroyImmediate(oldRotationFix);
			}
			gameObject.name = "PersistentRotationFix";
			// Persistant so only needed to set desired rotation once.
			DontDestroyOnLoad(this);
		}
	}

	/// <summary>
	/// Check users current settings when app is waked. Activity has focus.
	/// </summary>          
	private void Awake() {
		TestOrientation();
	}

	/// <summary>
	/// Check users current settings when app has regained focus if the user changes
	/// activity or goes outside the Unity Player i.e When user goes to settings bar
	/// and changes AutoRotate.
	/// </summary>
	/// <param name="focusStatus"> From Unity, true if the application has focus. </param>
	private void OnApplicationFocus(bool focusStatus) {
		if (focusStatus) {
			TestOrientation();
		}
	}

	/// <summary>
	///  Check if user has AutoRotate Enabled and change settings.
	/// </summary>
	private void TestOrientation() {
		if (AutoRotateUtil.IsAutoRotateEnabled()) {
			SetOrientationLock(allowedOrientation);
			ResetOrientation();
		}
		else {
			SetOrientationLock(AutoOrientation.kNoAuto);
			ResetOrientation();
		}
	}

	/// <summary>
	/// Set the autorotate settings for the screen.
	/// 
	/// Changes all the screen autorotate settings based on orientation setting.
	/// </summary>
	/// <param name="orientationSetting"></param>
	private void SetOrientationLock(AutoOrientation orientationSetting) {
		switch (orientationSetting) {
			case AutoOrientation.kAutoLandscape:
				Screen.autorotateToPortrait = false;
				Screen.autorotateToPortraitUpsideDown = false;
				Screen.autorotateToLandscapeLeft = true;
				Screen.autorotateToLandscapeRight = true;
				break;
			case AutoOrientation.kAutoPortrait:
				Screen.autorotateToPortrait = true;
				Screen.autorotateToPortraitUpsideDown = true;
				Screen.autorotateToLandscapeLeft = false;
				Screen.autorotateToLandscapeRight = false;
				break;
			case AutoOrientation.kAutoAll:
				Screen.autorotateToPortrait = true;
				Screen.autorotateToPortraitUpsideDown = true;
				Screen.autorotateToLandscapeLeft = true;
				Screen.autorotateToLandscapeRight = true;
				break;
			case AutoOrientation.kNoAuto:
				Screen.autorotateToPortrait = false;
				Screen.autorotateToPortraitUpsideDown = false;
				Screen.autorotateToLandscapeLeft = false;
				Screen.autorotateToLandscapeRight = false;
				break;
			default:
				break;
		}
		Screen.orientation = ScreenOrientation.AutoRotation;
	}

	/// <summary>
	/// Reset the autorotate settings for the screen.
	/// 
	/// Used for handling cases where Screen Orientation does not currently match 
	/// an allowed rotation but AutoRotate is disabled by the user.Ensures the
	/// screen is always in the correct rotation for the Unity Player.
	/// </summary>
	private void ResetOrientation() {
		switch (allowedOrientation) {
			case AutoOrientation.kAutoLandscape:
				if (Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown) {
					Screen.orientation = ScreenOrientation.LandscapeLeft;
				}
				break;
			case AutoOrientation.kAutoPortrait:
				if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight) {
					Screen.orientation = ScreenOrientation.Portrait;
				}
				break;
			default:
				break;
		}
		Screen.orientation = ScreenOrientation.AutoRotation;
	}
}
