﻿using UnityEngine;

/// <summary>
/// Changes the currently playing background music in the sound manager.
/// When this object is started it will change the background music once.
/// </summary>
public class BackgroundMusicChanger : MonoBehaviour {

	[SerializeField]
	AudioClip backgroundMusic = null;

	/// <summary>
	/// Plays the linked background music.
	/// </summary>
	private void Start() {
		SoundManager.PlayMusic(backgroundMusic);
	}
}
