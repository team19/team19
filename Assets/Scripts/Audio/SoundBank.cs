﻿using UnityEngine;

using LittleLot;


/// <summary>
/// Singleton SoundBank that manages specific audio.
/// </summary>
public class SoundBank : Singleton<SoundBank> {

	[SerializeField]
	AudioClip buttonClick = null;

	[SerializeField]
	AudioClip rotateSegmentClip = null;

	[SerializeField]
	AudioClip moveSegmentClip = null;

	[SerializeField]
	AudioClip collectableCollectedClip = null;

	[SerializeField]
	AudioClip starGainedClip = null;

	[SerializeField]
	AudioClip objectCompletedClip = null;

	[SerializeField]
	AudioClip revealClip = null;

	[SerializeField]
	AudioClip stampedClip = null;

	[SerializeField]
	AudioClip descendingMoveCountClip = null;

	/// <summary>
	/// Plays the SoundBanks Button click sfx.
	/// </summary>
	public static void PlayButtonClick() {
		if (instance) {
			SoundManager.PlaySfx(instance.buttonClick, 0.4f);
		}
	}

	/// <summary>
	/// Plays the Local SoundBanks Rotate Segment sfx.
	/// </summary>
	public static void PlayRotateSegment() {
		if (instance) {
			SoundManager.PlaySfx(instance.rotateSegmentClip);
		}
	}

	/// <summary>
	/// Plays the Local SoundBanks MoveSegment sfx.
	/// </summary>
	public static void PlayMoveSegment() {
		if (instance) {
			SoundManager.PlaySfx(instance.moveSegmentClip);
		}
	}

	/// <summary>
	/// Plays the Local SoundBanks Collectable sfx.
	/// </summary>
	public static void PlayCollectableCollected() {
		if (instance) {
			SoundManager.PlaySfx(instance.collectableCollectedClip);
		}
	}

	/// <summary>
	/// Plays the Local SoundBanks Star Gained sfx.
	/// </summary>
	public static void PlayStarGained() {
		if (instance) {
			SoundManager.PlaySfx(instance.starGainedClip);
		}
	}

	/// <summary>
	/// Plays the Local SoundBanks Object Completed sfx.
	/// </summary>
	public static void PlayObjectCompleted() {
		if (instance) {
			SoundManager.PlaySfx(instance.objectCompletedClip);
		}
	}

	/// <summary>
	/// Plays the Local SoundBanks Reveal sfx.
	/// </summary>
	public static void PlayReveal() {
		if (instance) {
			SoundManager.PlaySfx(instance.revealClip);
		}
	}

	/// <summary>
	/// Plays the Local SoundBanks stamped sfx.
	/// </summary>
	public static void PlayStamped() {
		if (instance) {
			SoundManager.PlaySfx(instance.stampedClip);
		}
	}

	/// <summary>
	/// Plays the Local SoundBanks Descending Count down, increase frequency to give effect of descending.
	/// </summary>
	public static void PlayDescendingCountDown(float pitch) {
		if (instance) {
			SoundManager.PlaySfx(instance.descendingMoveCountClip, 0.8f, pitch);
		}
	}
}
