﻿using UnityEngine;

using LittleLot;

/// <summary>
/// Singleton SoundManager that manages game audio in a persistant manor.
/// Handles multiple sfx's and a background track.
/// May be expanded to handle multiple bg tracks, and nice transitions from bg to bg track.
/// Uses PlayerPrefs to store Audio previous state. Used to decide if it can play an sfx/bg.
/// All functions will through a quiet error when no instance is found.
/// </summary>
public class SoundManager : Singleton<SoundManager> {

	[SerializeField]
	AudioSource backgroundMusic = null;

	[SerializeField]
	AudioSource[] sfxSources = new AudioSource[3];

	private int currentSfxSource =0;

	private bool musicEnabled_ = true;

	private bool sfxEnabled_ = true;

	private const float kMusicVolume_ = 0.2f;

	private const float kSfxVolume_ = 0.6f;

	private const string kMusicEnabledPlayerPrefName_ = "musicEnabled";

	private const string kSfxEnabledPlayerPrefName_ = "sfxEnabled";

	/// <summary>
	/// Ensures the soundManager isnt destroyed on load.
	/// </summary>
	private void Start() {
		DontDestroyOnLoad(gameObject);
	}

	/// <summary>
	/// Updates the music state.
	/// </summary>
	private void OnEnable() {
		musicEnabled_ = (PlayerPrefs.GetInt(kMusicEnabledPlayerPrefName_, 1) == 1);

		sfxEnabled_ = (PlayerPrefs.GetInt(kSfxEnabledPlayerPrefName_, 1) == 1);
	}

	/// <summary>
	/// Plays a sound clip at the provided volume scale.
	/// Uses any free sfxSource otherwise overrides oldest.
	/// </summary>
	/// <param name="clip"> Sound Clip to play. </param>
	/// <param name="volumeScale"> How much to amplify the Volume of the clip. Will be multiplied by SoundManagers sfxVolume. </param>
	/// <param name="pitchScale"> How much to modify the pitch by, 1.0f is no change. </param>
	public static void PlaySfx(AudioClip clip, float volumeScale = 1.0f, float pitchScale = 1.0f) {
		if (instance) {
			if (instance.sfxEnabled_) {
				instance.sfxSources[instance.currentSfxSource].Stop();
				instance.sfxSources[instance.currentSfxSource].pitch = pitchScale;
				instance.sfxSources[instance.currentSfxSource].PlayOneShot(clip, volumeScale * kSfxVolume_);
				instance.currentSfxSource++;
				if (instance.currentSfxSource >= instance.sfxSources.Length) {
					instance.currentSfxSource = 0;
				}
			}
		}
		else {
			LogNoInstance();
		}
	}

	/// <summary>
	/// Replaces the background music with the provided AudioClip.
	/// If audio is enabled will also start playing it.
	/// </summary>
	/// <param name="clip"> The new AudioClip to be played. </param>
	/// <param name="volumeScale"> The volume at which it should be played. Will be multiplied by SoundManagers musicVolume. </param>
	/// <param name="pitchScale"> The pitch the music should be played at. </param>
	public static void PlayMusic(AudioClip clip, float volumeScale = 1.0f, float pitchScale = 1.0f) {
		if (instance) {
			if (instance.backgroundMusic.clip != clip) {
				instance.backgroundMusic.volume = volumeScale * kMusicVolume_;
				instance.backgroundMusic.clip = clip;
				if (instance.musicEnabled_) {
					instance.backgroundMusic.Play();
				}
			}
		}
		else {
			LogNoInstance();
		}
	}

	/// <summary>
	/// Plays the previously playing background music.
	/// Only if music is enabled and a clip exists.
	/// </summary>
	public static void PlayPreviousMusic() {
		if (instance) {
			if (instance.backgroundMusic.clip && instance.musicEnabled_) {
				instance.backgroundMusic.Play();
			}
		}
		else {
			LogNoInstance();
		}
	}

	/// <summary>
	/// Pauses the background Music.
	/// </summary>
	public static void StopMusic() {
		if (instance) {
			instance.backgroundMusic.Pause();
		}
		else {
			LogNoInstance();
		}
	}

	/// <summary>
	/// Stops all currently running sfx.
	/// </summary>
	public static void StopAllSfx() {
		if (instance) {
			foreach (AudioSource sfxSource in instance.sfxSources) {
				sfxSource.Stop();
			}
		}
		else {
			LogNoInstance();
		}
	}

	/// <summary>
	/// Tells you if Music is enabled in the soundmanager.
	/// </summary>
	/// <returns> True if enabled, or no instance of the sound manager. </returns>
	public static bool IsMusicEnabled() {
		if (instance) {
			return instance.musicEnabled_;
		}
		else {
			LogNoInstance();
			return true;
		}
	}

	/// <summary>
	/// Tells you if Sfx is enabled in the soundmanager.
	/// </summary>
	/// <returns> True if enabled, or no instance of the sound manager. </returns>
	public static bool IsSfxEnabled() {
		if (instance) {
			return instance.sfxEnabled_;
		}
		else {
			LogNoInstance();
			return true;
		}
	}

	/// <summary>
	/// Enables the music to be played, and saves it to PlayerPrefs.
	/// Note: does not start playing the music.
	/// </summary>
	public static void EnableMusic() {
		if (instance) {
			instance.musicEnabled_ = true;
			PlayerPrefs.SetInt(kMusicEnabledPlayerPrefName_, 1);
		}
		else {
			LogNoInstance();
		}
	}

	/// <summary>
	/// Disables and stops the music being played, and saves it to PlayerPrefs.	
	/// </summary>
	public static void DisableMusic() {
		if (instance) {
			StopMusic();
			instance.musicEnabled_ = false;
			PlayerPrefs.SetInt(kMusicEnabledPlayerPrefName_, 0);
		}
		else {
			LogNoInstance();
		}
	}

	/// <summary>
	/// Enables Sfx allowing them to be played, and saves it to PlayerPrefs.
	/// </summary>
	public static void EnableSfx() {
		if (instance) {
			instance.sfxEnabled_ = true;
			PlayerPrefs.SetInt(kSfxEnabledPlayerPrefName_, 1);
		}
		else {
			LogNoInstance();
		}
	}

	/// <summary>
	/// Disables Sfx from playing, and stops all currently playing sfx, and saves it to PlayerPrefs.
	/// </summary>
	public static void DisableSfx() {
		if (instance) {
			StopAllSfx();
			instance.sfxEnabled_ = false;
			PlayerPrefs.SetInt(kSfxEnabledPlayerPrefName_, 0);
		}
		else {
			LogNoInstance();
		}
	}


	/// <summary>
	/// Logs that the SoundManager was not found in this scene.
	/// Usefull for when there should be an instance but can generally be ignored.
	/// </summary>
	private static void LogNoInstance() {
		Debug.Log("No Instance of Sound Manager, can generally be ignored if in editor.");
	}
}
