﻿using UnityEngine;

/// <summary>
/// Sets the orthographic camera size to the desired setting, dependent on aspect ratio of the current camera.
/// Allows seperate settings for each aspect ratio ensuring the full scene is viewable at all times.
/// </summary>
[RequireComponent(typeof(Camera))]
public class OrthoCameraCorrector : MonoBehaviour {

	private float fallbackOrthSize = 28;

	private float orthoSize16by9 = 21;

	private float orthoSize16by10 = 24;

	private float orthoSize3by2 = 25;

	private float orthoSize4by3 = 28;

	private Camera camera_ = null;

	/// <summary>
	/// Set to correct orthographic size on game start.
	/// </summary>
	private void Start() {
		OrthographicResize();
	}

	/// <summary>
	/// Allows the camera to update in editor when screen aspect ratio is changed.
	/// </summary>
	private void OnValidate() {
		OrthographicResize();
	}

	/// <summary>
	/// Resizes the orthographic size setting of the camera.
	/// Sets it to specific size dependant on aspect ratio.
	/// Allows seperate settings for ipad iphone etc.
	/// </summary>
	private void OrthographicResize() {
		if (!camera_) {
			camera_ = GetComponent<Camera>();
		}
		if (camera_.aspect >= 1.7) {
			camera_.orthographicSize = orthoSize16by9;
		}
		else if (camera_.aspect >= 1.6) {
			camera_.orthographicSize = orthoSize16by10;
		}
		else if (camera_.aspect >= 1.5) {
			camera_.orthographicSize = orthoSize3by2;
		}
		else if (camera_.aspect >= 1.3) {
			camera_.orthographicSize = orthoSize4by3;
		}
		else {
			Debug.Log("Fallback - Unknown Aspect");
			camera_.orthographicSize = fallbackOrthSize;
		}
	}
}
