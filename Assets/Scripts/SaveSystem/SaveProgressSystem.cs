﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using UnityEngine;

/// <summary>
///	SaveProgressSystem handles the save and loading of the games current state.
/// Uses singleton pattern for most recent save.
/// </summary>
public class SaveProgressSystem : MonoBehaviour {

	public const float currentSaveVersion = 0.15f;

	/// <summary>
	/// The most recent save that has been loaded into memory.
	/// </summary>
	private static SaveData recentSave_ = null;

	/// <summary>
	/// The most recent save loaded by the system.
	/// </summary>
	/// <returns></returns>
	public static SaveData RecentSave() {
		if (recentSave_ == null) {
			LoadProgress();
		}
		return recentSave_;
	}

	/// <summary>
	/// Attempts to override the save data for the scene.
	/// Save the provided data locally on the device, best to use RecentSave to access
	/// the most current savedata, then modify, instead of plain overriding.
	/// </summary>
	/// <param name="newSaveData"> Save Data to override with.</param>
	public static void SaveProgress(SaveData newSaveData) {
		// If it fails log it.
		try {
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = new FileStream(GetFileName(), FileMode.Create);
			newSaveData.version = currentSaveVersion;
			formatter.Serialize(stream, newSaveData);
			recentSave_ = newSaveData;
			stream.Close();
		}
		catch (Exception e) {
			Debug.LogWarning("Save Failed, Error: " + e.ToString());
		}

	}

	/// <summary>
	/// Loads the save data if it is available to be loaded and stores it in the System.	
	/// Otherwise creates empty data if there is none.
	/// Also Checks to see what version the save data was created under.
	/// If outdated it currently deletes the save and creates a new one.
	/// </summary>
	public static void LoadProgress() {
		// If it fails log it.
		try {
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = new FileStream(GetFileName(), FileMode.Open);
			recentSave_ = formatter.Deserialize(stream) as SaveData;
			if (recentSave_.version != currentSaveVersion) {
				Debug.LogWarning("Old Version Save File: Deleting SaveData and Creating Empty SaveData.");
				stream.Close();
				DeleteSave();
			}
			stream.Close();
		}
		catch (Exception e) {
			recentSave_ = new SaveData();
			Debug.LogWarning("Load Failed, Error: " + e.ToString());
			Debug.LogWarning("Creating Empty Save Data, Ignore if first run.");
		}
	}

	/// <summary>
	/// Display all available Save Data in the debug log.
	/// </summary>
	public static void LogAvailableSaveData() {
		RecentSave();
		bool hasData = false;
		Debug.Log("SaveData:");
		foreach (var item in recentSave_.availableLevelList) {
			hasData = true;
			Debug.Log("Scene: " + item.Key +
				" \nCollected: " + item.Value.itemsCollected +
				" \nMoveCount: " + item.Value.movesTaken +
				" \nItem Revealed: " + item.Value.isItemRevealed);

		}
		if (!hasData) {
			Debug.Log("\tNoData");
		}
	}

	/// /// <summary>
	/// Removes all the current save data, and sets recentSave to empty SaveData.
	/// </summary>
	public static void DeleteSave() {
		File.Delete(GetFileName());
		recentSave_ = new SaveData();
	}

	/// <summary>
	/// The location to store the save information.
	/// </summary>
	/// <returns> The path safe location to store the save. </returns>
	private static string GetFileName() {
		return Path.Combine(Application.persistentDataPath, "game_data.gg");
	}
}
