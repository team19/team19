﻿using System.Collections.Generic;

/// <summary>
/// Contains extra detail on a specific level.
/// </summary>
[System.Serializable]
public class LevelData {
	public int itemsCollected = 0;
	public int movesTaken= 0;
	public bool isItemRevealed = false;
}

/// <summary>
/// Contains data on all previously completed levels.
/// </summary>
[System.Serializable]
public class SaveData {

	/// <summary>
	/// The file version this was saved with, Allows us to check if it has changed and handle conversion.
	/// Note: Currently just deletes the old save and makes a new one.
	/// </summary>
	public float version = 0.0f;

	/// <summary>
	/// Scene,LevelData; contains a list of completed levels with their leveldata attached.
	/// </summary>
	public Dictionary<string,LevelData> availableLevelList = new Dictionary<string, LevelData>();

	public bool finishedOnboarding = false;
	public bool hasViewedEndscreen = false;

}
