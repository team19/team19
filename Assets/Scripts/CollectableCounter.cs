﻿using UnityEngine;

/// <summary>
/// Generates collectedPopUp [+1], also stores collectables collected.
/// </summary>
public class CollectableCounter : MonoBehaviour {

	[SerializeField]
	GameObject collectedPopupTemplate = null;

	private int numCollected_ = 0;

	/// <summary>
	/// Adds listeners to collection and game completion events.
	/// </summary>
	private void OnEnable() {
		EventManagerCollectable.OnSpawnPopUp += SpawnCollectablePopUp;
		EventManagerCollectable.OnItemCollected += IncreaseCollectedCount;
	}

	/// <summary>
	/// Removes all listeners.
	/// </summary>
	private void OnDisable() {
		EventManagerCollectable.OnSpawnPopUp -= SpawnCollectablePopUp;
		EventManagerCollectable.OnItemCollected -= IncreaseCollectedCount;
	}

	/// <summary>
	/// Ensures listeners are removed.
	/// </summary>
	private void OnDestroy() {
		this.OnDisable();
	}

	/// <summary>
	/// It creates a popup based on the collectablePopupTemplate.
	/// Also increments the number of collectables collected.
	/// </summary>
	/// <param name="worldPosition"> The world position of the collectable. </param>
	private void SpawnCollectablePopUp(Vector3 worldPosition) {

		SoundBank.PlayCollectableCollected();

		GameObject newPopup = Instantiate(collectedPopupTemplate);
		newPopup.transform.SetParent(transform, true);
		newPopup.transform.localScale = collectedPopupTemplate.transform.localScale;

		newPopup.transform.position = worldPosition;
		Vector3 rectPos = newPopup.GetComponent<RectTransform>().localPosition;
		rectPos.z = 0.0f;
		newPopup.GetComponent<RectTransform>().localPosition = rectPos;
		newPopup.gameObject.SetActive(true);
	}

	/// <summary>
	/// Increases the number collected.
	/// </summary>
	private void IncreaseCollectedCount() {
		numCollected_++;
	}
}
