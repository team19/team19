﻿using UnityEngine;

/// <summary>
/// Holder class for managing and controlling the Segments for one Design object.
/// Handles the changing of the order of child segments.
/// Also checks for overall completion of the object.
/// </summary>
public class DesignObject : MonoBehaviour {

	/// <summary>
	/// Using array as at run time the array will not be modified in size etc. Order of objects can be changed.
	/// In the editor script the array size is manipulated to determine the size and contents of the array.
	/// Ensuring it is static during game run time.
	/// </summary>
	[SerializeField]
	Segment[] segments = new Segment[0];

	/// <summary>
	/// Universal height of each segment.
	/// </summary>
	[SerializeField]
	float segmentHeight = 2.0f;

	/// <summary>
	/// Used to add a little leeway into the collision calculation.
	/// </summary>
	private float buffer_ = 0.2f;

	private bool hasPossibleCompletion_ = false;

	private bool isFinished =false;
	/// <summary>
	/// Add listeners to Segment movement, release events, and completion checks.
	/// </summary>
	private void OnEnable() {
		EventManagerSegment.OnSegmentMoveVertically += MoveSegment;
		EventManagerSegment.OnSegmentMoveReleased += RecalculateAllSegmentPositions;
		EventManagerSegment.OnSegmentMoveReleased += DelayedCheckForCompletion;
		EventManagerDesignObject.OnPossibleCompletion += DelayedCheckForCompletion;
		EventManagerDesignObject.OnCompletion += RemoveSegmentOutlines;
		RecalculateAllSegmentPositions();
	}

	/// <summary>
	/// Remove listeners to Segment movement, release events, and completion checks. 
	/// </summary>
	private void OnDisable() {
		EventManagerSegment.OnSegmentMoveVertically -= MoveSegment;
		EventManagerSegment.OnSegmentMoveReleased -= RecalculateAllSegmentPositions;
		EventManagerSegment.OnSegmentMoveReleased -= DelayedCheckForCompletion;
		EventManagerDesignObject.OnPossibleCompletion -= DelayedCheckForCompletion;
		EventManagerDesignObject.OnCompletion -= RemoveSegmentOutlines;
	}

	/// <summary>
	/// Make Sure all Event Listeners have been removed when destroying.
	/// </summary>
	private void OnDestroy() {
		this.OnDisable();
	}

	/// <summary>
	/// Draws Debug vertical collision wire cubes in the editor view.
	/// They represent the collision meshes for order movement and sorting.
	/// Only called in editor display.
	/// </summary>
	private void OnDrawGizmos() {
		for (int i = 0; i < segments.Length; i++) {
			Gizmos.color = Color.red;
			Segment tester = segments[i];
			Gizmos.DrawWireCube(new Vector3(tester.transform.position.x, tester.transform.position.y + (tester.transform.parent.localScale.y * (segmentHeight / 2)), tester.transform.position.z),
				new Vector3(10, (tester.transform.parent.localScale.y * segmentHeight), 10));
		}
	}

	/// <summary>
	/// Called after all other object updates have been called.
	/// This ensures all events for that update have been handled
	/// allowing the completion check to be done once per Update cycle.
	/// </summary>
	private void LateUpdate() {
		if (hasPossibleCompletion_ && !isFinished) {
			hasPossibleCompletion_ = false;
			if (IsObjectCompleted()) {
				isFinished = true;
				EventManagerDesignObject.DesignObjectCompleted();
				SoundBank.PlayObjectCompleted();
				RecalculateAllSegmentPositions();
			}
		}
	}

	/// <summary>
	/// Checking through each segment to see if it is completed.
	/// Will return false if any stage fails.
	/// </summary>
	public bool IsObjectCompleted() {
		for (int i = 0; i < segments.Length; i++) {
			if (!segments[i].InCorrectOrder(i)) {
				return false;
			}
			if (i < (segments.Length - 1)) {
				if (!segments[i].CheckRotation(segments[i + 1].Rotation())) {
					return false;
				}
			}
		}
		return true;    // only return true if everything is correct.
	}

	/// <summary>
	/// Recalculate the position of all segments. Placing them in the correct order dependent on the position in the array.
	/// </summary>
	public void RecalculateAllSegmentPositions() {
		float currentHeight = 0;
		for (int i = 0; i < segments.Length; i++) {
			segments[i].SetCurrentOrderNumber(i);
			currentHeight -= segmentHeight;
			segments[i].transform.localPosition = new Vector3(
				segments[i].transform.localPosition.x,
				currentHeight,
				segments[i].transform.localPosition.z);
		}
	}

	/// <summary>
	/// Checks the Design object for completion.
	/// But delayed to LateUpdate, so as not checking multiple times per frame.
	/// </summary>
	private void DelayedCheckForCompletion() {
		hasPossibleCompletion_ = true;
	}

	/// <summary>
	/// Removes the outline Resin around the segments.
	/// Allows this to be called on an event allowing it to happen when called form the debugger.
	/// </summary>
	private void RemoveSegmentOutlines() {
		// remove all the mesh renderers from the octagons.
		for (int i = 0; i < segments.Length; i++) {
			segments[i].GetComponent<MeshRenderer>().enabled = false;
		}
	}
	/// <summary>
	/// Matched to the OnSegmentMoveVertically event, therfore is called every time the segment moves vertically.
	/// Recalculates the desired position of all the objects.
	/// </summary>
	/// <param name="segmentBeingMoved"> The segment being moved by the user. </param>
	private void MoveSegment(Segment segmentBeingMoved) {
		int movedSeg = 0;

		// Points generated from expected position of object, not current dragged position.
		float collisionTop = 0;         // The collision point on the top of the selected segment.
		float collisionBottom = 0;      // The collision point on the bottom of the selected segment

		// Find an array refrence for the segment being moved. 
		// Used for swaping positions in the array properly.
		for (int i = 0; i < segments.Length; i++) {
			if (segments[i] == segmentBeingMoved) {
				movedSeg = i;
				break;
			}
			// Keep adding the segment heights together until we are at the moved segment.
			// Gives the collision point at the top of the moved segment. In relation to where it should be. 
			// Not where it currently is in world space.
			collisionTop -= segmentHeight;
		}
		// Calculate the bottom of the segment
		collisionBottom = collisionTop - segmentHeight;

		if (CanMoveDown(movedSeg)) {
			// Add a little leeway into the collision.
			collisionBottom -= buffer_;
			// Check below for collision.
			if (segmentBeingMoved.transform.localPosition.y < collisionBottom) {
				SwapSegment(movedSeg, movedSeg + 1);
			}
		}

		if (CanMoveUp(movedSeg)) {
			// Add a little leeway into the collision.
			collisionTop += buffer_;
			// Check above for collision.
			if (segmentBeingMoved.transform.localPosition.y > collisionTop) {
				SwapSegment(movedSeg, movedSeg - 1);
			}
		}

		// The segments have moved therefore recalculate there new positions in relation to the moving segment.
		RecalculateOtherSegmentPositions(segmentBeingMoved);
	}

	/// <summary>
	/// Segment can be swaped with an item in the array above it.
	/// Simple array bounds check.
	/// </summary>
	/// <param name="segmentBeingMoved"> Index refrence to the segment being moved. </param>
	/// <returns> True if it can be moved. </returns>
	private bool CanMoveUp(int segmentBeingMoved) {
		return segmentBeingMoved != 0;
	}

	/// <summary>
	/// Segment can be swaped with an item in the array below it.
	/// Simple array bounds check.
	/// </summary>
	/// <param name="segmentBeingMoved"> Index refrence to the segment being moved. </param>
	/// <returns> True if it can be moved. </returns>
	private bool CanMoveDown(int segmentBeingMoved) {
		return segmentBeingMoved < segments.Length - 1;
	}

	/// <summary>
	/// Move an array element from the source array position to the destination position,
	/// and move the destination element to the source position.
	/// <para> Source and destination should not match as pointless! </para>
	/// </summary>
	/// <param name="sourceSeg"> Index position to swap from. </param>
	/// <param name="destinationSeg"> Index position to swap with. </param>
	private void SwapSegment(int sourceSeg, int destinationSeg) {
		Segment temp = segments[destinationSeg];
		segments[destinationSeg] = segments[sourceSeg];
		segments[sourceSeg] = temp;
	}

	/// <summary>
	/// Recalculates the segments new positions in relation to the moving segment.
	/// Positioning all segments before the moving segment (in the array) on top of the moving segment.
	/// </summary>
	/// <param name="segmentBeingMoved">The segment being moved by the user. </param>
	private void RecalculateOtherSegmentPositions(Segment segmentBeingMoved) {

		// Index of array refrence to segment being moved.
		int movedSeg = 0;

		// Find an array refrence for the segment being moved. 
		// Used for swaping positions in the array properly.
		for (int i = 0; i < segments.Length; i++) {
			// Find self.
			if (segments[i] == segmentBeingMoved) {
				movedSeg = i;
				break;
			}
		}

		// Height to place segment at.
		float currentHeight = 0;
		// Iterate through all segments.
		for (int i = 0; i < segments.Length; i++) {
			// Add the height for segments even if not repositioned. Allows them to be placed in correct vertical position.
			currentHeight -= segmentHeight;
			// Only move segments not ontop of the Moved Segment.
			if (i > movedSeg) {
				segments[i].transform.localPosition = new Vector3(
					segments[i].transform.localPosition.x,
					currentHeight,
					segments[i].transform.localPosition.z);
			}

		}

		// Reposition the rest of the segments, above the segment being moved.
		currentHeight = segmentBeingMoved.transform.localPosition.y;
		for (int i = movedSeg; i >= 0; i--) {
			if (i != movedSeg) {
				segments[i].transform.localPosition = new Vector3(
					segments[i].transform.localPosition.x,
					currentHeight,
					segments[i].transform.localPosition.z);
			}
			currentHeight += segmentHeight;
		}
	}
}
