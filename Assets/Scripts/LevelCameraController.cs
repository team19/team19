﻿using UnityEngine;

/// <summary>
/// Moves the camera into win position on completion.
/// Plays the win animation of the camera into its win position on object completion.
/// </summary>
public class LevelCameraController : MonoBehaviour {

	[Tooltip("Where the camera will move to on completion, can be ignored when a tutorial camera.")]
	[SerializeField]
	Transform winCameraPosition = null;

	[Tooltip("How long the camera takes to move to the win position.")]
	[SerializeField]
	float transitionTime = 3;

	private bool isMovingToFinalPosition_ = false;

	private float startTime_ =0;

	private Vector3 initialPosition_ = Vector3.zero;

	/// <summary>
	/// Set to correct orthographic size on game start.
	/// </summary>
	private void Start() {
		initialPosition_ = transform.position;
		EventManagerDesignObject.OnCompletion += MoveCameraToWinPosition;
	}

	/// <summary>
	/// Removes the OnCompletion listener.
	/// </summary>
	private void OnDestroy() {
		EventManagerDesignObject.OnCompletion -= MoveCameraToWinPosition;
	}

	/// <summary>
	/// Moves the camera into win position using a smooth lerp.
	/// Done in lateUpdate due to being camera ensures it moves after everything has been 
	/// setup for rendering, reducing the chance of screen tear, and the appearance of
	/// jittering.
	/// </summary>
	private void LateUpdate() {
		if (isMovingToFinalPosition_) {
			transform.position = LittleLot.MathUtil.SmoothLerp(
					initialPosition_,
					winCameraPosition.position,
					startTime_,
					transitionTime,
					out isMovingToFinalPosition_);
			if (!isMovingToFinalPosition_) {
				EventManagerWinScreen.CameraInWinPosition();
				transform.position = winCameraPosition.position;
			}
		}
	}

	/// <summary>
	/// Starts the smooth lerp to the cameras win position.
	/// </summary>
	private void MoveCameraToWinPosition() {
		isMovingToFinalPosition_ = true;
		initialPosition_ = transform.position;
		startTime_ = Time.time;
	}
}
