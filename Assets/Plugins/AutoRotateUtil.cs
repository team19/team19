﻿using UnityEngine;

/// <summary>
/// Utility class to check if AutoRotation is allowed by the user.
/// 
/// Gives access to whether the user has AutoRotate turned on/off. This 
/// is useful for Unity as it does not track whether the user has disabled 
/// autoRotate itself.
/// </summary>
public class AutoRotateUtil {

#if UNITY_ANDROID && !UNITY_EDITOR
	private static AndroidJavaClass androidUnityActivity = null;

	/// <summary>    
	/// <para> Gets the current UnityActivity used on Android. </para>
	/// It will store the AndroidJavaClass for later use ensureing it is not 
	/// creating a new class in memory every call.
	/// </summary>
	/// <returns> The AndroidActivity with the UnityPlayer running in it. </returns>
	private static AndroidJavaObject GetUnityActivity() {
		if (androidUnityActivity == null) {
			androidUnityActivity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		}
		return androidUnityActivity.GetStatic<AndroidJavaObject>("currentActivity");
	}
#endif
	/// <summary>
	/// Tells you if the users auto rotation is enabled. Currently defaults to 
	/// true outside of android.
	/// TODO: Expos function for iOS.
	/// </summary>   
	/// <returns> Whether the user has Auto Rotate enabled or disabled. </returns>
	public static bool IsAutoRotateEnabled() {
		bool isAutoRotationEnabled = false;
#if UNITY_ANDROID && !UNITY_EDITOR
		using (AndroidJavaClass androidClass = new AndroidJavaClass("com.puzzld.detectautorotate.AutoRotateUtil")) {
			isAutoRotationEnabled = androidClass.CallStatic<bool>("IsAutoRotateEnabled", GetUnityActivity());
		}
#else


		// Temporary for other builds (IOS etc.)
		isAutoRotationEnabled = true;
#endif
		return isAutoRotationEnabled;
	}

}
