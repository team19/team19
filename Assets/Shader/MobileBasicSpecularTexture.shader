﻿/// <summary>
/// Basic Specular shader using texture for colour and specular, with alpha for gloss.
/// </summary>
Shader "BluePix/BasicSpecular" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_SpecularTex("Specular,  Gloss (A)", 2D) = "white" {}
	}
		SubShader{
			Tags{ "RenderType" = "Opaque" }
			LOD 250

			CGPROGRAM
		#pragma surface surf MobileBlinnPhong exclude_path:prepass nolightmap noforwardadd halfasview interpolateview

			inline fixed4 LightingMobileBlinnPhong(SurfaceOutput s, fixed3 lightDir, fixed3 halfDir, fixed atten) {
				fixed diff = max(0, dot(s.Normal, lightDir));
				fixed nh = max(0, dot(s.Normal, halfDir));
				fixed spec = pow(nh, s.Specular * 128) * s.Gloss;

				fixed4 c;
				c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
				UNITY_OPAQUE_ALPHA(c.a);
				return c;
			}

			sampler2D _MainTex;
			sampler2D _SpecularTex;

			struct Input {
				float2 uv_MainTex;
				float2 uv_SpecularTex;
			};

			void surf(Input IN, inout SurfaceOutput o) {
				fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
				fixed4 spec = tex2D(_SpecularTex, IN.uv_SpecularTex);
				o.Albedo = tex.rgb;
				o.Gloss = spec.a;
				o.Alpha = tex.a;
				o.Specular = spec.a;
			}
			ENDCG
	}
		FallBack "Mobile/VertexLit"
}
