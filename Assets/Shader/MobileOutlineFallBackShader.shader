﻿/// <summary>
/// Mobile Outliner fall back shader.
/// Renders the mesh with transparency still changes colour on move/select.
/// </summary>
Shader "BluePix/MobileOutlineFallBack" {
	Properties{
		_MainColor("Main Color", Color) = (1,0.765,0.588,0.25)
		_OutlineColor("Main Outline Color", Color) = (1,0.404,0,1)

		_SelectedColor("Selected Color", Color) = (0.173,1,0.098,0.25)
		_SelectedOutlineColor("Selected Outline Color", Color) = (0.173,1,0.098)

		_MovingColor("Selected Color", Color) = (0.392,0.031,0.62,0.25)
		_MovingOutlineColor("Moving Outline Color", Color) = (0.392,0.031,0.62)

		// [MaterialToggle] allows it to be shown as a tickbox, in the unity editor,
		// but it is still not a bool in the shader. Requires SetFloat in object script
		// and >0 <0 checks in shader.
		[MaterialToggle] _isMoving("isMoving", Float) = 0

		// [MaterialToggle] allows it to be shown as a tickbox, in the unity editor,
		// but it is still not a bool in the shader. Requires SetFloat in object script
		// and >0 <0 checks in shader.
		[MaterialToggle] _isSelected("isSelected", Float) = 0
	}

	SubShader{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }

		CGPROGRAM
		#pragma surface surf Lambert alpha
		struct Input {
			float2 uv_MainTex;
		};
		uniform float4 _MainColor;
		uniform float4 _SelectedColor;
		uniform float4 _MovingColor;
		float _isMoving;
		float _isSelected;

		/// <summary>
		/// Surface shader override, renders the main body of the mesh with a lambert function.
		/// Using the current states albedo and alpha values.
		/// </summary>
		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = _MainColor;
			o.Alpha = _MainColor.a;
			if (_isSelected > 0) {
				o.Albedo = _SelectedColor;
				o.Alpha = _SelectedColor.a;
			}
			if (_isMoving > 0) {
				o.Albedo = _MovingColor;
				o.Alpha = _MovingColor.a;
			}
		}
		ENDCG
	}

	FallBack "VertexLit"
}