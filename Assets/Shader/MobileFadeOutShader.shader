﻿/// <summary>
/// Mobile Fade Out Shader.
/// Fades out the object when faded/disabled.
/// </summary>
Shader "BluePix/MobileFadeOut" {
	Properties{

		_MainTex("Base (RGB)", 2D) = "white" {}
		_FadedColor("Selected Color", Color) = (0.173,1,0.098,0.25)

		// [MaterialToggle] allows it to be shown as a tickbox, in the unity editor,
		// but it is still not a bool in the shader. Requires SetFloat in object script
		// and >0 <0 checks in shader.
		[MaterialToggle] _isFaded("isFaded", Float) = 0
	}

	SubShader{
		Tags{ "Queue" = "Transparent-1" "RenderType" = "Transparent" }

		CGPROGRAM
		#pragma surface surf Lambert alpha
		struct Input {
			float2 uv_MainTex;
		};

		sampler2D _MainTex;

		uniform float4 _FadedColor;

		float _isFaded;

		/// <summary>
		/// Surface shader override, renders the main body of the mesh with a lambert function.
		/// Using the main colour and its alpha, unless faded out uses the faded color an its alpha.
		/// </summary>
		void surf(Input IN, inout SurfaceOutput o) {
			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = tex.rgb;
			o.Alpha = tex.a;
			if (_isFaded > 0) {
				o.Albedo = _FadedColor;
				o.Alpha = _FadedColor.a;
			}
		}
		ENDCG
	}

	Fallback "BluePix/MobileOutlineFallBack"
}