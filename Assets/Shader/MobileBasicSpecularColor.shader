﻿/// <summary>
/// Basic Specular shader using base colour, with alpha for gloss.
/// </summary>
Shader "BluePix/BasicSpecularColour" {
	Properties{
		_Shininess("Shininess", Range(0.03, 1)) = 0.078125
		_MainColor("Base (RGB) Gloss (A)", Color) = (1,1,1,1)
	}
		SubShader{
			Tags{ "RenderType" = "Opaque" }
			LOD 250

			CGPROGRAM
		#pragma surface surf MobileBlinnPhong exclude_path:prepass nolightmap noforwardadd halfasview interpolateview

			inline fixed4 LightingMobileBlinnPhong(SurfaceOutput s, fixed3 lightDir, fixed3 halfDir, fixed atten) {
				fixed diff = max(0, dot(s.Normal, lightDir));
				fixed nh = max(0, dot(s.Normal, halfDir));
				fixed spec = pow(nh, s.Specular * 128) * s.Gloss;

				fixed4 c;
				c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
				UNITY_OPAQUE_ALPHA(c.a);
			return c;
			}

			struct Input {
				float2 uv_MainTex;
			};

			uniform float4 _MainColor;
			half _Shininess;

			void surf(Input IN, inout SurfaceOutput o) {
				fixed4 tex = _MainColor;
				o.Albedo = tex.rgb;
				o.Gloss = tex.a;
				o.Alpha = tex.a;
				o.Specular = _Shininess;
			}
			ENDCG
	}
		FallBack "Mobile/VertexLit"
}
