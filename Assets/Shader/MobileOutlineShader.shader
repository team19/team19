// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

/// <summary>
/// Mobile Outliner shader.
/// Generates model faces offset by depth offset, to create outline effect.
/// </summary>
Shader "BluePix/MobileOutline" {
	Properties{

		_MainColor("Main Color", Color) = (1,0.765,0.588,0.25)
		_OutlineColor("Main Outline Color", Color) = (1,0.404,0,1)

		_SelectedColor("Selected Color", Color) = (0.173,1,0.098,0.25)
		_SelectedOutlineColor("Selected Outline Color", Color) = (0.173,1,0.098)

		_MovingColor("Selected Color", Color) = (0.392,0.031,0.62,0.25)
		_MovingOutlineColor("Moving Outline Color", Color) = (0.392,0.031,0.62)


		// [MaterialToggle] allows it to be shown as a tickbox, in the unity editor,
		// but it is still not a bool in the shader. Requires SetFloat in object script
		// and >0 <0 checks in shader.
		[MaterialToggle] _isMoving("isMoving", Float) = 0

		// [MaterialToggle] allows it to be shown as a tickbox, in the unity editor,
		// but it is still not a bool in the shader. Requires SetFloat in object script
		// and >0 <0 checks in shader.
		[MaterialToggle] _isSelected("isSelected", Float) = 0
	}

	SubShader{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		/// <summary>
		/// First pass cull the back faces and cut out the centre for the outline.		
		/// </summary>
		Pass{
			Name "BASE"
			Cull Back
			// Cuts out the centre of the object.
			Blend Zero One
			// Offsets the depth buffer rendering.
			Offset 2, 1
		}
		/// <summary>
		/// Second Pass cull the front faces
		/// </summary>
		Pass{
			Name "OUTLINE"
			Tags{ "LightMode" = "Always" }
			Cull Front
			ZWrite Off
			Offset -2, -1

			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 pos : POSITION;
				float4 color : COLOR;
			};

			uniform float4 _OutlineColor;
			uniform float4 _SelectedOutlineColor;
			uniform float4 _MovingOutlineColor;
			float _isMoving;
			float _isSelected;

			/// <summary>
			/// The vertex shader override, positions the vertex and sets it to outline color.
			/// </summary>
			v2f vert(appdata v) {
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.color = _OutlineColor;
				if (_isSelected > 0) {
					o.color = _SelectedOutlineColor;
				}
				if (_isMoving > 0) {
					o.color = _MovingOutlineColor;
				}

				return o;
			}
			/// <summary>
			/// Fragment shader override only want to render the outlines colour to the screen.
			/// </summary>
			half4 frag(v2f i) :COLOR {
				return i.color;
			}
			ENDCG
		}

		CGPROGRAM
		#pragma surface surf Lambert alpha
		struct Input {
			float2 uv_MainTex;
		};
		uniform float4 _MainColor;
		uniform float4 _SelectedColor;
		uniform float4 _MovingColor;
		float _isMoving;
		float _isSelected;

		/// <summary>
		/// Surface shader override, renders the main body of the mesh with a lambert function.
		/// Using the current states albedo and alpha values.
		/// </summary>
		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = _MainColor;
			o.Alpha = _MainColor.a;
			if (_isSelected > 0) {
				o.Albedo = _SelectedColor;
				o.Alpha = _SelectedColor.a;
			}
			if (_isMoving > 0) {
				o.Albedo = _MovingColor;
				o.Alpha = _MovingColor.a;
			}

		}
		ENDCG
	}

	Fallback "BluePix/MobileOutlineFallBack"
}